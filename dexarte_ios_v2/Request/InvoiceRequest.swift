//
//  InvoiceRequest.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/21/21.
//

import Foundation



class InvoiceRequest {
    static func call_request(param: [String:String], completion_handler : @escaping(String) ->()){
    
        print(get_url())
      
        BaseRequest.get(url: get_url(), param: param).success{
            
             
            (res) in
            completion_handler(res as! String)
        }
    }
    
    
    private static func get_url() -> String{
        return String(format: "%@/invoice",Constants.BASE_URL )
    }
}
