//
//  PlaceOrderRequest.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/23/21.
//

import Foundation



class PlaceOrderRequest {
    
    static func call_request(param : String, completion_handler : @escaping(String) -> () ) {
        
        BaseRequest.raw_post(url: get_url(), param: param) .success {
            (res) in
            completion_handler(res as! String)
        }
    }
    
    
    private static func get_url() -> String {
        return String (format: "%@/place_order", Constants.BASE_URL)
        
    }
}
