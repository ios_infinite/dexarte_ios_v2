//
//  ProductDetailRequest.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/16/21.
//

import Foundation


class ProductDetailRequest {
    
    static func call_request(param : [String:String],completion_handler : @escaping(String) ->()) {
        
        BaseRequest.get(url: get_url(), param: param).success{
            (res) in
            completion_handler(res as! String)
        }
        
    }
    
    private static func get_url() -> String{
        return String(format: "%@/product_detail", Constants.BASE_URL )
    }
    
}







   
