//
//  LoginRequest.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/18/21.
//

import Foundation
import KRProgressHUD


class LoginRequest {

    static func call_request(param : [String:String],completion_handler : @escaping(String) ->()) {

        KRProgressHUD.show()
       BaseRequest.get(url: get_url(), param: param).success{
            (res) in
            completion_handler(res as! String)
        }

    }

    private static func get_url() -> String{
        return String(format: "%@/login", Constants.BASE_URL )
    }

}
