//
//  ProductSpecificationRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/16/21.
//

import UIKit

class ProductSpecificationRowItem: UITableViewCell {

    @IBOutlet weak var lbl_product_sheet: UILabel!
    @IBOutlet weak var lbl_product_thickness: UILabel!
    @IBOutlet weak var lbl_specification: UILabel!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var img_layer: UIImageView!
    
    @IBOutlet weak var lbl_thickness: UILabel!
    @IBOutlet weak var lbl_size: UILabel!
    @IBOutlet weak var lbl_desc1: UILabel!
    @IBOutlet weak var lbl_desc2: UILabel!
    @IBOutlet weak var lbl_desc3: UILabel!
    
    @IBOutlet weak var lbl_recommend: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        lbl_product_sheet.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        lbl_product_thickness.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        lbl_specification.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
//      lbl_description.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        lbl_size.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        lbl_thickness.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        lbl_recommend.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
    }
    
}
