//
//  ProductLayerRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductLayerRowItem: UITableViewCell {

    @IBOutlet weak var img_layer: UIImageViewX!
    @IBOutlet weak var lbl_first_info: UILabel!
    @IBOutlet weak var lbl_second_info: UILabel!
    @IBOutlet weak var lbl_third_info: UILabel!
    @IBOutlet weak var img_second_dot: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func load_info(_ layer: ProductLayer){
        img_second_dot.isHidden = layer.layer_count == 2
        img_layer.image = UIImage(named: layer.image)
        lbl_first_info.text = layer.description[0]
        lbl_second_info.text = layer.description[1]
        lbl_third_info.text = layer.description[2]
    }
    
}
