//
//  ProductFeatureRow.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductFeatureRow: UITableViewCell {

    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var cv_height: NSLayoutConstraint!
    
    let row_identifier = "ProductFeatureRowItem"
    var features: [ProductFeature] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cv.dataSource = self
        cv.delegate = self
        
        cv.register(UINib(nibName: row_identifier, bundle: nil), forCellWithReuseIdentifier: row_identifier)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func load_features(_ features: [ProductFeature]){
        self.features = features
        cv.reloadData()
    }
    
}

extension ProductFeatureRow : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return features.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: row_identifier, for: indexPath) as! ProductFeatureRowItem
        let feature: ProductFeature = features[indexPath.item]
        cell.img_feature.image = UIImage(named: feature.image)
        cell.lbl_name.text = feature.title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2.0, height: 150.0)
    }
}
