//
//  ProductFeaturesRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/16/21.
//

import UIKit

class ProductFeaturesRowItem: UITableViewCell,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    let row_identifier = "FeaturesRowItem"
    var product_feature : [ProductFeature] = []
    
    @IBOutlet weak var cv_bottom: NSLayoutConstraint!
    @IBOutlet weak var cv: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cv.dataSource = self
        cv.delegate = self
        
        let row_nib = UINib(nibName: row_identifier, bundle: nil)
        cv.register(row_nib, forCellWithReuseIdentifier: row_identifier)
        
        cv.reloadData()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product_feature.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: row_identifier, for: indexPath) as! FeaturesRowItem
      //let feature : ProductFeature = product_feature[indexPath.row]
        cell.img_icon.image = UIImage(named: product_feature[indexPath.row].image)
        cell.lbl_title.text = product_feature[indexPath.row].title
//        print(product_feature[indexPath.row].title!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView
                        .frame.size.width / 2, height: 150)
    }
    
}
