//
//  FeaturesRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/26/21.
//

import UIKit

class FeaturesRowItem: UICollectionViewCell {

    @IBOutlet weak var img_icon: UIImageView!
    
    @IBOutlet weak var lbl_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

    lbl_title.set_font(font_type: .FRIGHT_LIGHT, font_size: 15.0)
    
 }

}
