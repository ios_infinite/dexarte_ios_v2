//
//  ProductImagesRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductImagesRowItem: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_first: UIImageViewX!
    @IBOutlet weak var img_second: UIImageViewX!
    @IBOutlet weak var img_third: UIImageViewX!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization codeR
        
        img_first.isUserInteractionEnabled = true
        img_second.isUserInteractionEnabled = true
        img_third.isUserInteractionEnabled = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func load_images(_ img1_url: String, _ img2_url: String, _ img3_url: String) {
        img_first.sd_setImage(with: URL(string: img1_url))
        img_second.sd_setImage(with: URL(string: img2_url))
        img_third.sd_setImage(with: URL(string: img3_url))
    }
    
}
