//
//  ProductCartRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductCartRowItem: UITableViewCell {
    @IBOutlet weak var view_minus: UIView!
    @IBOutlet weak var view_plus: UIView!
    @IBOutlet weak var lbl_qty: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
