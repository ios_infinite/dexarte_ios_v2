//
//  ProductDetailRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/16/21.
//

import UIKit
import ImageScrollView


class ProductDetailRowItem: UITableViewCell {

    @IBOutlet weak var img_slider1: UIImageView!
    @IBOutlet weak var img_slider2: UIImageView!
    @IBOutlet weak var img_slider3: UIImageView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lbl_product_title: UILabel!
    
    
    @IBOutlet weak var view_zoom: UIViewX!
    
    @IBOutlet weak var view_icon: UIView!
    @IBOutlet weak var lbl_details: UILabel!
    @IBOutlet weak var view_minus: UIView!
    @IBOutlet weak var view_add: UIView!
    
    
    var img_cart : ProductDetailsVC!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        lbl_product_title.set_font(font_type: .ENCORPADA, font_size: 35.0)
        lbl_product_title.update_space(line_space: 0, letter_space: 1.0)
        
    }
    
   

    
}


