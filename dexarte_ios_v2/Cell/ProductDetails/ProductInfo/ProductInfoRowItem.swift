//
//  ProductInfoRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductInfoRowItem: UITableViewCell {

    @IBOutlet weak var lbl_product_size: UILabel!
    @IBOutlet weak var lbl_product_thickness: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
