//
//  ProductImagePreviewRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductImagePreviewRowItem: UITableViewCell {

    @IBOutlet weak var img_preview: UIImageViewX!
    
    @IBOutlet weak var view_zoom: UIViewX!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func load_image(_ img_url: String){
        img_preview.sd_setImage(with: URL(string: img_url))
    }
    
}
