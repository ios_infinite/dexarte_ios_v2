//
//  ProductFeatureRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/7/21.
//

import UIKit

class ProductFeatureRowItem: UICollectionViewCell {

    @IBOutlet weak var img_feature: UIImageViewX!
    @IBOutlet weak var lbl_name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
