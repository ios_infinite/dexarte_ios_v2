//
//  ContactCallRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 02/07/21.
//

import UIKit

class ContactCallRowItem: UITableViewCell {

    @IBOutlet weak var view_whatsapp: UIView!
    @IBOutlet weak var view_call: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
