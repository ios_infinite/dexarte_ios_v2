//
//  ContactLocationRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 02/07/21.
//

import UIKit

class ContactLocationRowItem: UITableViewCell {
    
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var lbl_heading: UILabel!
    
    @IBOutlet weak var view_email: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 20.0)
        
        lbl_heading.set_font(font_type: .FRIGHT_LIGHT, font_size: 20.0)
        
    }
    
}
