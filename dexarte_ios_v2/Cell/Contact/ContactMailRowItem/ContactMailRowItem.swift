//
//  ContactMailRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 02/07/21.
//

import UIKit
import SkyFloatingLabelTextField

class ContactMailRowItem: UITableViewCell {

    @IBOutlet weak var lbl_name: SkyFloatingLabelTextField!
    
    @IBOutlet weak var lbl_email: SkyFloatingLabelTextField!
    @IBOutlet weak var lbl_send: UILabel!
    
    @IBOutlet weak var lbl_message: SkyFloatingLabelTextField!
    @IBOutlet weak var lbl_phone: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        lbl_send.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
    }
    
}
