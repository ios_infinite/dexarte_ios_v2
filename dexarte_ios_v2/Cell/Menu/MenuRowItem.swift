//
//  MenuRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 05/07/21.
//

import UIKit

class MenuRowItem: UITableViewCell {

    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var img_icon: UIImageView!
    
    @IBOutlet weak var lbl_title: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
