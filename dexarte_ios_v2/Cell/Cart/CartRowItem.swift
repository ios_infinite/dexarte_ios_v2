//
//  CartRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/23/21.
//

import UIKit

class CartRowItem: UITableViewCell {

    @IBOutlet weak var view_delete : UIView!
    @IBOutlet weak var img_icon  : UIImageView!
    @IBOutlet weak var lbl_id: UILabel!
    
    @IBOutlet weak var view_minus: UIView!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var view_add: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        let add_gesture = CountGesture(target: self, action: #selector(count))
//        add_gesture.type = 1
//        view_add.addGestureRecognizer(add_gesture)
        
        let minus_gesture = CountGesture(target: self, action: #selector(count))
        minus_gesture.type = -1
        view_minus.addGestureRecognizer(minus_gesture)
        
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
    @IBAction func count(gesture : CountGesture) {

        var count = lbl_count.text!

        if count.elementsEqual(""){
            count = "0"
        }
        if gesture.type == 1{

            lbl_count.text = String(Int(count)! + 1)

        }else{
            if !count.elementsEqual("0"){
                lbl_count.text = String(Int(count)! - 1)
            }
        }
        
        
    }
    
    
   
}


class CountGesture : UITapGestureRecognizer{
    
    var type : Int!
    
}
