//
//  InvoiceRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/22/21.
//

import UIKit

class InvoiceRowItem: UITableViewCell {

    
    @IBOutlet weak var lbl_order_number: UILabel!
    @IBOutlet weak var lbl_invoice_no: UILabel!
    @IBOutlet weak var lbl_invoice_date: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    
    @IBOutlet weak var view_track: UIViewX!
    @IBOutlet weak var lbl_invoice_company: UILabel!
    @IBOutlet weak var lbl_igst: UILabel!
    @IBOutlet weak var lbl_cgst: UILabel!
    @IBOutlet weak var lbl_sgst: UILabel!
    @IBOutlet weak var lbl_amount: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
