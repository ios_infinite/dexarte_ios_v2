//
//  HomeRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 23/06/21.
//

import UIKit

class HomeRowItem: UICollectionViewCell {

    @IBOutlet weak var view_enter: UIView!
    @IBOutlet weak var img_enter: UIImageView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var lbl_sub_title: UILabel!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lbl_btn: UILabel!
    @IBOutlet weak var view_button: UIView!
    
    @IBOutlet weak var sub_title_height: NSLayoutConstraint!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 30.0)
        lbl_title.update_space(line_space: 9.0, letter_space: 7.0)
        lbl_sub_title.set_font(font_type: .FRIGHT_LIGHT, font_size: 15.0)
        lbl_sub_title.update_space(line_space: 5.0, letter_space: 5.0)
        lbl_btn.set_font(font_type: .FRIGHT_LIGHT, font_size: 12.0)
        lbl_btn.update_space(line_space: 1.0, letter_space: 5.0)
    }
}
