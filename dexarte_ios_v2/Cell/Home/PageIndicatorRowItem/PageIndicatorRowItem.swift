//
//  PageIndicatorRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 23/06/21.
//

import UIKit

class PageIndicatorRowItem: UITableViewCell {

    
    @IBOutlet weak var view_page: UIView!
    
    @IBOutlet weak var page_indicator_width: NSLayoutConstraint!
    @IBOutlet weak var lbl_page: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        lbl_page.set_font(font_type: .FRIGHT_BIG, font_size: 20.0)
    }
    
}
