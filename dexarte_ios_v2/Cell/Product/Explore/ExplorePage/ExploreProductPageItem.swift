//
//  ExploreProductPageItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/5/21.
//

import UIKit

class ExploreProductPageItem: UICollectionViewCell {

    
    @IBOutlet weak var img_product: UIImageView!
    
    @IBOutlet weak var lbl_product_title: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
