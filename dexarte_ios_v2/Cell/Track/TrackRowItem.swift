//
//  TrackRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/22/21.
//

import UIKit

class TrackRowItem: UITableViewCell {

    
    @IBOutlet weak var lbl_order_no: UILabel!
    @IBOutlet weak var lbl_invoice_no: UILabel!
    @IBOutlet weak var lbl_invoice_date: UILabel!
    
    @IBOutlet weak var view_invoice: UIViewX!
    @IBOutlet weak var view_challan: UIViewX!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
