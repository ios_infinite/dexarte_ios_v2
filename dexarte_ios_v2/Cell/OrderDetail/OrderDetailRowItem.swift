//
//  OrderDetailRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/2/21.
//

import UIKit

class OrderDetailRowItem: UITableViewCell {

    @IBOutlet weak var lbl_category_name: UILabel!
    @IBOutlet weak var lbl_product_code: UILabel!
    @IBOutlet weak var lbl_qty: UILabel!
    @IBOutlet weak var lbl_status: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}




