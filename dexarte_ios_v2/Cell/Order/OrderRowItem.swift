//
//  OrderRowItem.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/1/21.
//

import UIKit

class OrderRowItem: UITableViewCell {

    
    
    @IBOutlet weak var lbl_date: UILabel!
    
    @IBOutlet weak var order_no: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
