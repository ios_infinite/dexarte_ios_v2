//
//  AboutTrophyRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 30/06/21.
//

import UIKit

class AboutTrophyRowItem: UICollectionViewCell {

    @IBOutlet weak var lbl_paragraph: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl_paragraph.set_font(font_type: .FRIGHT_BIG, font_size: 15.0)
        lbl_paragraph.update_space(line_space: 2.0, letter_space: 1.0)
    }

}
