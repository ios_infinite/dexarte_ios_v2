//
//  AboutFlowerRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 30/06/21.
//

import UIKit

class AboutFlowerRowItem: UICollectionViewCell {

    
    @IBOutlet weak var lbl_title: UILabel!
    
    
    @IBOutlet weak var lbl_content: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 20)
        lbl_title.update_space(line_space: 5.0, letter_space: 1.0)
        lbl_content.set_font(font_type:.ENCORPADA, font_size: 10)
        lbl_content.update_space(line_space: 10.0, letter_space: 2.0)
    }

}
