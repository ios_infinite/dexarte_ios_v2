//
//  AboutPdfRowItem.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 30/06/21.
//

import UIKit

class AboutPdfRowItem: UICollectionViewCell {

    
    
    @IBOutlet weak var lbl_download: UILabel!
    
    @IBOutlet weak var view_pdf: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        lbl_download.set_font(font_type: .ENCORPADA , font_size: 20.0)
        lbl_download.update_space(line_space: 1.0, letter_space: 1.0)
        
    }

}
