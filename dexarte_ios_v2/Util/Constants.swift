//
//  Constants.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/5/21.
//

import Foundation


final class Constants {
    static let BASE_URL : String = "http://dexarte.co.in/rest/api/"
    static let CART : String = "CART"
    static let LOGIN_RESPONSE : String = "login_response"
//    static let Product_Response : SString = "product_response"
 
    static let CART_RESPONSE : String = "cart_response"
    
    static let INVOICE_RESPONSE : String = "invoice_response"
}
