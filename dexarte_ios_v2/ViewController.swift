//
//  ViewController.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 23/06/21.
//

import UIKit
import FAPanels

class ViewController: UIViewController {

    @IBOutlet weak var img_logo: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        img_logo.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
        
        UIView.animate(withDuration: 1.0, animations: {
            self.img_logo.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }, completion: { _ in
            
            let main : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            let menu_panel : FAPanelController = main.instantiateViewController(identifier: "FAPanelController") as! FAPanelController
            let menu_vc = MenuVC(nibName: nil, bundle: nil)
            let welcome_vc = WelcomeVC(nibName: nil, bundle: nil)
            menu_panel.left(menu_vc).center(welcome_vc)
            
            menu_panel.leftPanelPosition = .front
            menu_panel.configs.leftPanelWidth = self.view.frame.width
            menu_panel.configs.centerPanelTransitionType = .moveUp
           
            
            menu_panel.modalPresentationStyle = .fullScreen
            self.present(menu_panel, animated: true, completion: nil)
        })
    }

}

