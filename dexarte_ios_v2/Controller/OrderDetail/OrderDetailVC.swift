//
//  OrderDetailVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/2/21.
//


import UIKit
import ObjectMapper

class OrderDetailVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var view_back: UIView!
    
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var lbl_product: UILabel!
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var lbl_order_no: UILabel!
    
    var order_product_list : [OrderProductDetail] = []
    var id : String!
    
    let row_identifier = "OrderDetailRowItem"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let row_nib = UINib(nibName: row_identifier, bundle: nil)
        tv.register(row_nib, forCellReuseIdentifier: row_identifier)
       
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
         order_detail_list()
    }

    
    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back (){
//        let vc = WelcomeVC(nibName: nil, bundle: nil)
//          panel?.center(vc)
        dismiss(animated: true, completion: nil)
        }
    func order_detail_list() {
        
        let param : [String:String] = ["order_id" : id]
        print(id)

              OrderDetailRequest.call_request(param: param) { [self]
            (res) in
            let order_list : OrderDetailResponse = Mapper <OrderDetailResponse>().map(JSONString: res)!
            order_product_list = order_list.product
            
            self.lbl_date.text = order_list.order_date
            self.lbl_order_no.text = order_list.order_no
            self.lbl_product.text =  "\(String(order_list.product.count)) Products"
            
            lbl_heading.set_font(font_type: .ENCORPADA, font_size: 25.0)
            lbl_heading.update_space(line_space: 5.0, letter_space: 5.0)
            
            print(order_list.product!)
            
            tv.dataSource = self
            tv.delegate =  self
            tv.reloadData()
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order_product_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: row_identifier, for: indexPath) as! OrderDetailRowItem
        
        let order : OrderProductDetail = order_product_list[indexPath.row]
        cell.lbl_category_name.text = order.packages_category_name
        cell.lbl_product_code.text = order.package_title
        cell.lbl_qty.text = order.request_qty
        cell.lbl_status.text = order.status
       return cell
    }
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 160
    }

}
