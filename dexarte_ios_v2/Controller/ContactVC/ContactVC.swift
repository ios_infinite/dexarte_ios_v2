//
//  ContactVC.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 02/07/21.
//

import UIKit

class ContactVC: UIViewController,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate {
    
    

    @IBOutlet weak var view_back: UIView!
    
    
    @IBOutlet weak var tv: UITableView!
   
    
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var lbl_subtitle: UILabel!
    
    
    let mail_row_identifier = "ContactMailRowItem"
    let location_row_identifier = "ContactLocationRowItem"
    let call_row_identifier = "ContactCallRowItem"
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
     
        
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 20)
        lbl_title.update_space(line_space: 5.0, letter_space: 1.0)
        
        lbl_subtitle.set_font(font_type: .FRIGHT_LIGHT, font_size: 15.0)
        lbl_subtitle.update_space(line_space: 5.0, letter_space: 1.0)
        
        
        tv.dataSource = self
        tv.delegate = self
        
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        
        let mail_row_nib = UINib(nibName: mail_row_identifier, bundle: nil)
        tv.register(mail_row_nib, forCellReuseIdentifier: mail_row_identifier)
        
        let location_row_nib = UINib(nibName: location_row_identifier, bundle: nil)
        tv.register(location_row_nib, forCellReuseIdentifier: location_row_identifier)
        let call_row_nib = UINib(nibName: call_row_identifier, bundle: nil)
        tv.register(call_row_nib, forCellReuseIdentifier: call_row_identifier)
        
        
    }

    @IBAction func back(){
        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: mail_row_identifier, for: indexPath)as! ContactMailRowItem
            cell.lbl_email.delegate = self
            cell.lbl_message.delegate = self
            cell.lbl_name.delegate = self
            cell.lbl_phone.delegate = self
            return cell
        } else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: location_row_identifier, for: indexPath)as! ContactLocationRowItem
            let email_gesture = UITapGestureRecognizer(target: self, action: #selector(email))
            cell.view_email.addGestureRecognizer(email_gesture)
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: call_row_identifier, for: indexPath)as! ContactCallRowItem
            
            let whatsapp_gesture = UITapGestureRecognizer(target: self, action: #selector(whatsapp_url))
            cell.view_whatsapp.addGestureRecognizer(whatsapp_gesture)
            
         
//            
           let call_gesture = UITapGestureRecognizer(target: self, action: #selector(call_url))
            cell.view_call.addGestureRecognizer(call_gesture)
            return cell
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   

    
    @IBAction func whatsapp_url() {
        
        
        let whatsappURL = URL(string: "https://api.whatsapp.com/send?phone=9819779947&text=Invitation")
        if UIApplication.shared.canOpenURL(whatsappURL!) {
            UIApplication.shared.open(whatsappURL!, options: [:], completionHandler: nil)
           }
    
        let urlWhats = "whatsapp://send?phone=+919819779947&abid=12354&text=Hello"
            if let urlString = urlWhats.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) {
                if let whatsappURL = URL(string: urlString) {
                    if UIApplication.shared.canOpenURL(whatsappURL) {
                        UIApplication.shared.openURL(whatsappURL)
                    } else {
                        print("Install Whatsapp")
                    }
                }
            }
    }
    @IBAction func call_url (){
        let url = URL(string: "tel://022-26760091")
        UIApplication.shared.open(url!,options: [:],completionHandler: nil)
    }
    
    @IBAction func email() {
        let url = URL(string: "mail to://INFO@DEXARTE.CO.IN")
        UIApplication.shared.open(url!,options: [:],completionHandler: nil)
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 300
        }else if indexPath.row == 1 {
            return 330
        }
        return 100.0
    }
    
}
