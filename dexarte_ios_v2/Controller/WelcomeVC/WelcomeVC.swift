//
//  WelcomeVC.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 23/06/21.
//

import UIKit
import AVFoundation
import FAPanels


class WelcomeVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var view_play: UIView!
    @IBOutlet weak var img_volume: UIImageView!
    @IBOutlet weak var view_menu: UIView!
    
    let row_identifier  = "HomeRowItem"
    let page_indicator_row_identifier = "PageIndicatorRowItem"
    
    var current_page : Int = 1
    var player: AVAudioPlayer?
    var ismute : Bool!
    
    let items : [ImageItem] =
        [
            ImageItem(image_icon: "ic_welcome_screen", page_number: 1, title: "WELCOME\n TO\n DEXARTE", sub_title: "SUPERIOR QUALITY DECORATIVE PANELS", btn: "ENTER", type: .ENTER),
            ImageItem(image_icon: "ic_about", page_number: 2, title: "REDEFINING\n LUXURY\n LIVING", sub_title: "", btn: "ABOUT US", type: .ABOUT_US),
            ImageItem(image_icon: "ic_product", page_number: 3, title: " DESIGN THAT TRANSFORM YOUR HOME ", sub_title: "",  btn: "OUR PRODUCTS", type: .OUR_PRODUCTS),
            ImageItem(image_icon: "ic_review", page_number: 4, title: " BEAUTIFUL RESULTS ", sub_title: "", btn: "REVIEWS & PRESS", type: .REVIEW_PRESS),
            ImageItem(image_icon: "ic_contact", page_number: 5, title: " SEND US\n A MESSAGE ", sub_title: "", btn: "CONTACT US", type: .CONTACT_US)
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cv.dataSource = self
        cv.delegate = self
        
        tv.delegate = self
        tv.dataSource = self
        
        let row_nib = UINib(nibName: row_identifier, bundle: nil)
        cv.register(row_nib, forCellWithReuseIdentifier: row_identifier)
        
        let page_indicator_nib = UINib(nibName: page_indicator_row_identifier, bundle: nil)
        tv.register(page_indicator_nib, forCellReuseIdentifier: page_indicator_row_identifier)
        
        
        let playAudio_gesture = UITapGestureRecognizer(target: self, action: #selector(mute_volume))
                view_play.addGestureRecognizer(playAudio_gesture)

        playAudioFile()

        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
    }
    
    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: row_identifier, for: indexPath)as! HomeRowItem
        let item : ImageItem = items[indexPath.item]
        
        cell.img_enter.image = UIImage(named: item.image_icon)
        cell.lbl_title.text = item.title
        cell.lbl_sub_title.text = item.sub_title
        
        cell.lbl_btn.text = item.btn
        let view_button_gesture = ViewButtonGesture(target: self, action: #selector(redirect_vc))
        view_button_gesture.type = item.type
        cell.view_button.addGestureRecognizer(view_button_gesture)
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let page_height = scrollView.frame.size.height
        let current_index = Int(floor((scrollView.contentOffset.y - page_height / 2) / page_height) + 2)
        print(current_index)
        current_page = current_index
        tv.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.size.height)
    }
    
    @IBAction func redirect_vc(_ sender : ViewButtonGesture){
        let type: ImageItem.ImageType = sender.type
        
        switch type{
        case .ENTER :
            cv.scrollToItem(at: IndexPath(item: 1, section: 0), at: .centeredVertically, animated: true)
            current_page = 2
            tv.reloadData()
            break
        case .ABOUT_US :
            let about_vc = AboutVC(nibName: nil, bundle: nil)
            panel?.center(about_vc)
            about_vc.modalPresentationStyle = .fullScreen
//            present(about_vc, animated: true, completion: nil)
            break
        case .OUR_PRODUCTS:
            let product_vc = ProductVC(nibName: nil, bundle: nil)
            panel?.center(product_vc)
            product_vc.modalPresentationStyle = .fullScreen
//            present(product_vc, animated: true, completion: nil)
            break
        case .REVIEW_PRESS:
            let review_vc = ReviewVC(nibName: nil, bundle: nil)
            panel?.center(review_vc)
            review_vc.modalPresentationStyle = .fullScreen
//            present(review_vc, animated: true, completion: nil)
            break
        case .CONTACT_US:
            let contact_vc = ContactVC(nibName: nil, bundle: nil)
            panel?.center(contact_vc)
            contact_vc.modalPresentationStyle = .fullScreen
//            present(contact_vc, animated: true, completion: nil)
            break
        }
    }
    
    
    
    @objc func playAudioFile() {
            ismute = true
            let path = Bundle.main.path(forResource: "river-2.mp3", ofType:nil)!
            let url = URL(fileURLWithPath: path)

            do {
                player = try AVAudioPlayer(contentsOf: url)
                player?.play()
            } catch {
        
            }
        
        }
        

      @IBAction func mute_volume(){
    
    if ismute == true {
        self.img_volume.image = UIImage(named: "ic_mute")
        player?.volume = 0.0
        ismute = false

    }else{
        self.img_volume.image = UIImage(named: "ic_volume")
        player?.volume = 1.0
        ismute = true

    }
    
      }
        
    
}


extension WelcomeVC : UITableViewDataSource,UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: page_indicator_row_identifier , for: indexPath)as! PageIndicatorRowItem
        
        
        let item : ImageItem = items[indexPath.row]
        cell.lbl_page.text = String(item.page_number)
        
        
        if current_page == (indexPath.row + 1){
            cell.page_indicator_width.constant = 20.0
        }else{
            cell.page_indicator_width.constant = 10.0
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        current_page = indexPath.row + 1
        cv.scrollToItem(at: indexPath, at: .centeredVertically, animated: true)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
}

struct ImageItem {
    
    enum ImageType {
        case ENTER, ABOUT_US, OUR_PRODUCTS, REVIEW_PRESS, CONTACT_US
    }
    
    var image_icon  : String
    var page_number : Int
    var title       : String
    var sub_title   : String
    var btn         : String
    var type        : ImageType
}

class ViewButtonGesture: UITapGestureRecognizer{
    var type: ImageItem.ImageType!
}
