//
//  PlaceOrderVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/23/21.
//

import UIKit
import ObjectMapper

class PlaceOrderVC: UIViewController {

    var login_response : LoginResponse!
    
//    var cart_product : [CartModel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let str_login_response = PreferencesUtil.getFromPrefs(key: Constants.LOGIN_RESPONSE)
        login_response = Mapper<LoginResponse>().map(JSONString: str_login_response)
        
    }

    func place_order() {
        
        
//        let str_product : String = cart_response.product_id
//        let str_qty    : Int = cart_response.quantity
////        let str_email  : String  = order_response.email
//
//
//        let place_order_param = PlaceOrderModel()
////        place_order_param.product = str_product
//
        
        var productList : [ProductList] = []
                var placeOrderModel : PlaceOrderModel!

        let cart_product = CartModel.cart_list()
        
        for i in 0...cart_product.count{
                    let productData = ProductList()
            productData.product_id = cart_product[i].product_id
            productData.qty = String(cart_product[i].quantity)
            productData.email = login_response.detail.email
                    
                    productList.append(productData)
                    print("item1\(productList)")
                    
        }
                
            
        placeOrderModel = PlaceOrderModel.init(pro: productList)
                
                let json_str1 = placeOrderModel.toJSONString()!
                print("item2\(json_str1)")
        
        
        PlaceOrderRequest.call_request(param: json_str1){
            (res) in
            print(res)
            let order_response : PlaceOrderResponse = Mapper<PlaceOrderResponse>().map(JSONString: res)!
            
            
        }
        
    }
   
}





