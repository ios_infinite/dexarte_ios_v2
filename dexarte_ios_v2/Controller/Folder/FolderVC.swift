//
//  FolderVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/20/21.
//

import UIKit

class FolderVC: UIViewController {

    @IBOutlet weak var view_back: UIView!
    
    @IBOutlet weak var view_folder: UIView!
    
    @IBOutlet weak var view_menu: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        view_folder.alpha = 1
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
    }


    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back (){
        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
        }

}
