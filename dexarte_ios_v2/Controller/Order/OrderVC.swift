//
//  OrderVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/1/21.
//

import UIKit
import ObjectMapper


class OrderVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
  
   
    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var lbl_order_count: UILabel!
    
    
    var order_list : [OrderResponse] = []
    let order_row_idenitifier = "OrderRowItem"
    var order_count : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let order_row_nib = UINib(nibName: order_row_idenitifier, bundle: nil)
        tv.register(order_row_nib, forCellReuseIdentifier: order_row_idenitifier)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
        order_value()
        
        lbl_heading.set_font(font_type: .ENCORPADA, font_size: 25.0)
        lbl_heading.update_space(line_space: 3.0, letter_space: 5.0)
        lbl_order_count.text = order_count
        tv.reloadData()
        
    }
    
    
    
    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back() {
        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
        dismiss(animated: true, completion: nil)
   }
    func order_value(){
        let login_response = PreferencesUtil.getFromPrefs(key: Constants.LOGIN_RESPONSE)
        let login : LoginResponse = Mapper<LoginResponse>().map(JSONString: login_response)!
        
        let email = login.detail.email!
        let param : [String : String] = [ "email" : email]
        
        OrderRequest.call_request(param: param) { [self]
            (res) in
            let order_response : [OrderResponse] = Mapper<OrderResponse>().mapArray(JSONString: res)!
            order_list = order_response
            tv.dataSource = self
            tv.delegate = self
            tv.reloadData()
        
//            self.view.makeToast("Order Placed", duration: 0.5)
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order_list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: order_row_idenitifier, for: indexPath) as! OrderRowItem
        let order : OrderResponse = order_list[indexPath.row]
        cell.lbl_date.text = order.date_created
        print(order.date_created!)
        cell.order_no.text = "#\(String(order.order_number))"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let order : OrderResponse = order_list[indexPath.row]
        let vc = OrderDetailVC(nibName: nil, bundle: nil)
        vc.id = order.order_number
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
}
