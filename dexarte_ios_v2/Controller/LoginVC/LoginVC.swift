//
//  LoginVC.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 06/07/21.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper

class LoginVC: UIViewController,UITextFieldDelegate {
    
    
    @IBOutlet weak var lbl_title: UILabel!
    
    @IBOutlet weak var login_view_height: NSLayoutConstraint!
    @IBOutlet weak var login_view: UIViewX!
    
    @IBOutlet weak var view_close: UIViewX!
    @IBOutlet weak var text_email: SkyFloatingLabelTextField!
    @IBOutlet weak var text_password: SkyFloatingLabelTextField!
    @IBOutlet weak var img_eye: UIImageView!
    @IBOutlet weak var view_eye: UIView!
    @IBOutlet weak var lbl_sub_title: UILabel!
    @IBOutlet weak var view_login: UIView!
    
  
    var callback : (() -> Void)?
    
    var show_password = true
    var iconClick = true

    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        text_email.delegate = self
        text_password.delegate = self
        
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 20.0)
        lbl_sub_title.set_font(font_type: .ENCORPADA, font_size: 20.0)
        
        let close_gesture = UITapGestureRecognizer(target: self, action: #selector(cancel))
        view_close.addGestureRecognizer(close_gesture)
        
        let eye_gesture = UITapGestureRecognizer(target: self, action: #selector(password))
        view_eye.addGestureRecognizer(eye_gesture)
        
        
        let login_gesture = UITapGestureRecognizer(target: self, action: #selector(login))
        view_login.addGestureRecognizer(login_gesture)
  
    }
   
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
   
    
    
    @IBAction func password(){
        print("password working")
        show_password = !show_password
        text_password.isSecureTextEntry = show_password
        img_eye.image = UIImage(named: show_password ?  "ic_eye" : "ic_hidden" )

    }
    
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        UIView.animate(withDuration: 0.3, animations: {
            self.view.frame = CGRect(x:self.view.frame.origin.x, y:self.view.frame.origin.y, width:self.view.frame.size.width, height:self.view.frame.size.height);

        })
    }

    
    @IBAction func cancel(){
     
            let vc = WelcomeVC(nibName: nil, bundle: nil)
              panel?.center(vc)
        dismiss(animated: true, completion: nil)
        }
    
    @IBAction func login(){
        
        let str_username = text_email.text!
        let str_password = text_password.text!
        
        
        if str_username.isEmpty{
            _ = SweetAlert().showAlert("Warning",subTitle: "Email required",style: .warning,buttonTitle: "Ok",buttonColor: UIColor(named: "App Color")!)
            print("username")
        }else if str_password.isEmpty{
            _ = SweetAlert().showAlert("Warning",subTitle: "Password required",style: .warning,buttonTitle: "Ok",buttonColor: UIColor(named: "App Color")!)
            print("password")
        }else{
            
            
            let param : [String:String] =
                [

                    "email" : str_username,
                    "password" : str_password

                ]

            LoginRequest.call_request(param: param){

                (res) in

                
                let login_response : LoginResponse = Mapper<LoginResponse>().map(JSONString: res)!

                if login_response.status.elementsEqual("0"){
                    _ = SweetAlert().showAlert("Warning",subTitle: "Wrong username or password",style: .warning,buttonTitle: "Ok",buttonColor: .green)
                    print(login_response.response!)
                    self.callback!()
                    
                }else{
                    
                    PreferencesUtil.saveToPrefs(key: Constants.LOGIN_RESPONSE, value: res)
                    
                    self.view.makeToast(login_response.response, duration: 0.5)
                    let vc = WelcomeVC(nibName: nil, bundle: nil)
                    vc.modalPresentationStyle = .fullScreen
                    self.panel?.center(vc)

            }
            
        }
        
    }
    }
}
