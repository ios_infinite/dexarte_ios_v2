//
//  InvoiceVc.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/21/21.
//

import UIKit
import Alamofire
import ObjectMapper


class InvoiceVc: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var view_back: UIView!
    
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var lbl_heading: UILabel!
    
    @IBOutlet weak var view_invoice: UIView!
    @IBOutlet weak var tv: UITableView!
    
    
    var invoice_item : [InvoiceResponse] = []
    
    let invoice_row_identifier = "InvoiceRowItem"
    var login_response : LoginResponse!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_invoice.alpha = 0
        
        let invoice_row_nib = UINib(nibName: invoice_row_identifier, bundle: nil)
        tv.register(invoice_row_nib, forCellReuseIdentifier: invoice_row_identifier)
        
        let str_login_response = PreferencesUtil.getFromPrefs(key: Constants.LOGIN_RESPONSE)
        login_response = Mapper<LoginResponse>().map(JSONString: str_login_response)
        
        invoice()
        lbl_heading.update_space(line_space: 1.0, letter_space: 2.0)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
    }
    
    
    
    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back (){
        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
        }
    func invoice() {
        let email = login_response.detail.email!
        let param : [String  : String] = ["email" : "maakarniply@gmail.com"]
        
        InvoiceRequest.call_request(param: param) { [self]
            (res) in
            
            let invoice_response: [InvoiceResponse] = Mapper<InvoiceResponse>().mapArray(JSONString: res)!
            invoice_item = invoice_response
            tv.dataSource =  self
            tv.delegate =  self
            tv.reloadData()
        }
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return invoice_item.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: invoice_row_identifier, for: indexPath) as! InvoiceRowItem
        
        let invoice : InvoiceResponse = invoice_item[indexPath.row]
        cell.lbl_order_number.text = String(format: "#%@", invoice.order_number)
        print()
        cell.lbl_invoice_no.text = String(format: "#%@", invoice.invoice_number)
        cell.lbl_invoice_date.text = invoice.date_created
        cell.lbl_status.text =  invoice.status
        cell.lbl_invoice_company.text = invoice.invoice_company
        cell.lbl_igst.text = "\(String(invoice.igst)) %"
        cell.lbl_cgst.text = "\(String(invoice.cgst)) %"
        cell.lbl_sgst.text = "\(String(invoice.sgst)) %"
        cell.lbl_amount.text = String(format: "RS.%@", invoice.amount)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 440
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let invoice_detail : InvoiceResponse = invoice_item[indexPath.row]
        
        let vc = TrackVC(nibName: nil, bundle: nil)
        vc.modalPresentationStyle = .fullScreen
        vc.order_no = invoice_detail.order_number
        vc.invoice_no = invoice_detail.invoice_number
        vc.invoice_date = invoice_detail.date_created
        
        present(vc, animated: true, completion: nil)
        
    }
    
}
