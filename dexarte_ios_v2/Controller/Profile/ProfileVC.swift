//
//  ProfileVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/20/21.
//

import UIKit

class ProfileVC: UIViewController {

    @IBOutlet weak var lbl_heading: UILabel!
    @IBOutlet weak var view_details: UIViewX!
    @IBOutlet weak var view_back: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        lbl_heading.update_space(line_space: 1.0, letter_space: 2.0)
        
        view_details.shadowOpacity = 2
        view_details.shadowColor = .gray
        view_details.shadowRadius = 2
        view_details.shadowOffsetY = 2
        
        let back_bgesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_bgesture)
    }


    @IBAction func back() {
        let vc = WelcomeVC(nibName: nil, bundle: nil)
        panel?.center(vc)
        dismiss(animated: true, completion: nil)
    }
    
}
