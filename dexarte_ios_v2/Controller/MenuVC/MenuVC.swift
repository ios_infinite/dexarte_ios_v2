//
//  MenuVC.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 02/07/21.
//

import UIKit
import FAPanels


class MenuVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var tv: UITableView!
    
    @IBOutlet weak var view_cancel: UIView!
    
    var menu_item : [Menu] = getMenuItems()
    
    
   let menu_row_identifier = "MenuRowItem"
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
 
        tv.dataSource = self
        tv.delegate = self
        
        
        let menu_row_nib = UINib(nibName: menu_row_identifier, bundle: nil)
        tv.register(menu_row_nib, forCellReuseIdentifier: menu_row_identifier)
        
        let cancel_gesture = UITapGestureRecognizer(target: self, action: #selector(cancel))
        view_cancel.addGestureRecognizer(cancel_gesture)
    }
    
    @IBAction func cancel(){
        panel?.closeLeft()

    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        menu_item.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: menu_row_identifier, for: indexPath) as! MenuRowItem
        let item : Menu = menu_item[indexPath.row]
        cell.img_icon.image = UIImage(named: item.menu_icon)
        cell.lbl_title.text = item.menu_title
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item : Menu = menu_item[indexPath.row]
        switch item.menu_type {
        case .HOME:
            let home_vc = WelcomeVC(nibName: nil, bundle: nil)
            panel?.center(home_vc)
            break
       
        case .PRODUCTS:
            let product_vc = ProductVC(nibName: nil, bundle: nil)
            panel?.center(product_vc)
            break
        case .CART:
            let cart_vc = CartVC(nibName: nil, bundle: nil)
            panel?.center(cart_vc)
            break
        case .ABOUTUS:
            let about_vc = AboutVC(nibName: nil, bundle: nil)
            panel?.center(about_vc)
            break
        case .NEWSTESTIMONIALS:
            let news_vc = ReviewVC(nibName: nil, bundle: nil)
            panel?.center(news_vc)
            break
        case .CONTACT:
            let contact_vc = ContactVC(nibName: nil, bundle: nil)
            panel?.center(contact_vc)
            break
       
        case .ORDER:
            let order_vc = OrderVC(nibName: nil, bundle: nil)
            panel?.center(order_vc)
            break
        case .FOLDER:
            let folder_vc = FolderVC(nibName: nil, bundle: nil)
            panel?.center(folder_vc)
            break
        case .INVOICE:
            let invoice_vc = InvoiceVc(nibName: nil, bundle: nil)
            panel?.center(invoice_vc)
            break
        case .PROFILE:
            let profile_vc = ProfileVC(nibName: nil, bundle: nil)
            panel?.center(profile_vc)
            break
        case .LOGIN:
                    let loginVC = LoginVC(nibName: nil, bundle: nil)
                    loginVC.modalPresentationStyle = .fullScreen
                    loginVC.callback = {
                        self.menu_item = getMenuItems()
                        self.tv.reloadData()
                    }
//                self.menu_item = getMenuItems()
                    panel?.center(loginVC)
                    
                    break
        case .LOGOUT:
            PreferencesUtil.removePrefs(key: Constants.LOGIN_RESPONSE)
            view.makeToast("Logout Success")
            DispatchQueue.main.asyncAfter(deadline: .now()+0.5){
                
//                let vc = WelcomeVC(nibName: nil, bundle: nil)
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
                
                self.panel?.closeLeft()
                self.menu_item = getMenuItems()
                self.tv.reloadData()
            }

            break
        }
    }
    
    
  


    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            if menu_item[indexPath.row].menu_show == true{
                return 50.0
            }else{
                return 0.0
            }
    }

}

func getVisibility() -> Bool{
    if PreferencesUtil.checkPrefs(key: Constants.LOGIN_RESPONSE) == true{
        return true
    }
    return false
}

struct Menu{
    
    enum MenuType{
        case HOME,PRODUCTS,CART,ORDER,FOLDER,INVOICE,ABOUTUS,NEWSTESTIMONIALS,CONTACT,PROFILE,LOGOUT,LOGIN
    }
    let menu_title: String
    let menu_show : Bool
    let menu_icon : String
    let menu_type : MenuType
    
}

func getMenuItems() -> [Menu]{
    
    let menu_item : [Menu] =
        [

            Menu.init(menu_title: "HOME",  menu_show: true,menu_icon: "ic_home", menu_type: Menu.MenuType.HOME),
            Menu.init(menu_title: "PRODUCTS", menu_show: true, menu_icon: "ic_copy", menu_type: Menu.MenuType.PRODUCTS),
            Menu.init(menu_title: "CART", menu_show: true, menu_icon: "ic_shopping-cart", menu_type: Menu.MenuType.CART),
            Menu.init(menu_title: "ORDER", menu_show: getVisibility() ? true : false, menu_icon: "ic_shopping-bag", menu_type: Menu.MenuType.ORDER),
            Menu.init(menu_title: "FOLDER", menu_show: getVisibility() ? true : false, menu_icon: "ic_folder", menu_type: Menu.MenuType.FOLDER),
            Menu.init(menu_title: "INVOICE", menu_show: getVisibility() ? true : false, menu_icon: "ic_invoice", menu_type: Menu.MenuType.INVOICE),
            Menu.init(menu_title: "ABOUT US", menu_show: true, menu_icon: "ic_user", menu_type: Menu.MenuType.ABOUTUS),
            Menu.init(menu_title: "NEWS & TESTIMONIALS", menu_show: true, menu_icon: "ic_newspaper", menu_type: Menu.MenuType.NEWSTESTIMONIALS),
            Menu.init(menu_title: "CONTACT", menu_show: true, menu_icon: "ic_email", menu_type: Menu.MenuType.CONTACT),
            Menu.init(menu_title: "PROFILE", menu_show: getVisibility() ? true : false, menu_icon: "ic_login ", menu_type: Menu.MenuType.PROFILE),
            Menu.init(menu_title: "LOGOUT", menu_show: getVisibility() ? true : false, menu_icon: "ic_logout", menu_type: Menu.MenuType.LOGOUT),
            Menu.init(menu_title: "LOGIN", menu_show: getVisibility() ? false : true, menu_icon: "ic_login ", menu_type: Menu.MenuType.LOGIN)
            
        ]
    return menu_item
    
}
