//
//  ProductDetailVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/31/21.
//

import Foundation
//
//  ProductDetailsVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/16/21.
//

import UIKit
import Alamofire
import ObjectMapper
import SDWebImage

class ProductDetailsVC: UIViewController {
    
    @IBOutlet weak var img_arrow: UIImageView!
    @IBOutlet weak var tv_height: NSLayoutConstraint!
    @IBOutlet weak var lbl_cart: UILabel!
    
    @IBOutlet weak var view_shop_cart: UIView!
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var lbl_count: UILabel!
    @IBOutlet weak var lbl_order: UILabel!
    @IBOutlet weak var img_cart: UIImageView!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var view_order: UIViewX!
    
    var product_detail : ProductDetailResponse!
    var product_id : String!
    var category_type: String!
    var category_description : String!
    var titleName : String!
    var sel_img_url: String!
    var cart_list : [CartModel] = []
    var detail_items: [ProductItem] = []
    var cart_qty: String = "0"
    
    let images_row_item = "ProductImagesRowItem"
    let image_preview_row_item = "ProductImagePreviewRowItem"
    let cart_row_item = "ProductCartRowItem"
    let info_row_item = "ProductInfoRowItem"
    let layer_row_item = "ProductLayerRowItem"
    let adhesive_row_item = "ProductAdhesiveRowItem"
    let feature_row_item = "ProductFeatureRow"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        img_cart.alpha = 0
        lbl_count.alpha = 0
        lbl_cart.alpha = 0
        
        lbl_cart.layer.cornerRadius = 9
        lbl_cart.layer.masksToBounds = true
        
        lbl_order.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        lbl_count.set_font(font_type: .FRIGHT_LIGHT, font_size: 20.0)
        lbl_cart.set_font(font_type: .FRIGHT_LIGHT, font_size: 17.0)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let shop_cart_gesture = UITapGestureRecognizer(target: self, action: #selector(cart))
        view_shop_cart.addGestureRecognizer(shop_cart_gesture)
        
        let order_cart_gesture = UITapGestureRecognizer(target: self, action: #selector(cart))
        view_order.addGestureRecognizer(order_cart_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 85, right: 0)
        
        tv.register(UINib(nibName: images_row_item, bundle: nil), forCellReuseIdentifier: images_row_item)
        tv.register(UINib(nibName: image_preview_row_item, bundle: nil), forCellReuseIdentifier: image_preview_row_item)
        tv.register(UINib(nibName: cart_row_item, bundle: nil), forCellReuseIdentifier: cart_row_item)
        tv.register(UINib(nibName: info_row_item, bundle: nil), forCellReuseIdentifier: info_row_item)
        tv.register(UINib(nibName: layer_row_item, bundle: nil), forCellReuseIdentifier: layer_row_item)
        tv.register(UINib(nibName: adhesive_row_item, bundle: nil), forCellReuseIdentifier: adhesive_row_item)
        tv.register(UINib(nibName: feature_row_item, bundle: nil), forCellReuseIdentifier: feature_row_item)
        
        detail_items.append(ProductItem(type: .PRODUCT_IAMGES))
        detail_items.append(ProductItem(type: .IMAGE_PREVIEW))
        detail_items.append(ProductItem(type: .CART))
        detail_items.append(ProductItem(type: .INFO))
        detail_items.append(ProductItem(type: .LAYER))
        detail_items.append(ProductItem(type: .ADHESIVE))
        detail_items.append(ProductItem(type: .FEATURE))
        
        cart_count_detail()
        change_cart_style()
        product_details()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        change_cart_style()
        product_details()
        tv.reloadData()
    }
    
    func product_details() {
        let param : [String : String] = ["id" : product_id]
        ProductDetailRequest.call_request(param: param) { [self]
            (res) in
            let detail : ProductDetailResponse = Mapper<ProductDetailResponse>().map(JSONString: res)!
            print("id \(product_id!)")
            product_detail  = detail
            sel_img_url = product_detail.get_slider_image(img_url: product_detail.slider_image1)
            let cart_product = CartModel.get_cart(product_id: product_detail.id)
            if cart_product.quantity != nil{
                cart_qty = String(cart_product.quantity)
                
            }else{
                cart_qty = "0"
            }
            change_cart_style()
            tv.dataSource = self
            tv.delegate = self
            tv.reloadData()
        }
    }
    
   @IBAction func menu() {
    panel?.openCenter(animated: true)
    }
    
    func cart_count_detail(){
        let cart_count = CartModel.cart_count()
        var qty: Int = Int(cart_qty)!
        if cart_count.elementsEqual("0"){

            lbl_cart.alpha = 0
        }else{
            lbl_cart.alpha = 1
        }
    }
    
    @IBAction func back(){
//        let vc = ExploreVC(nibName: nil, bundle: nil)
//        panel?.center(vc)
//        vc.modalPresentationStyle = .fullScreen
//        present(vc, animated: true, completion: nil)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cart() {
        let vc = CartVC(nibName: nil, bundle: nil)

        vc.icon = product_detail.slider_image1
        vc.product_id = product_detail.id
        vc.product_name = product_detail.package_title
        vc.product_image = product_detail.slider_image1
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func zoom() {
        let vc = ZoomVC(nibName: nil, bundle: nil)
        vc.img_icon = product_detail.get_slider_image(img_url: product_detail.slider_image1)
//        print(vc.img_icon)
//        vc.img_icon = product_detail.slider_image1
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func add_cart(){
        var qty: Int = Int(cart_qty)!
        
        if qty >= 0{
            qty += 1
            cart_count_detail()
            update_cart(qty: qty)
        }
        change_cart_style()
        cart_count_detail()
    }
    
    @IBAction func remove_cart(){
        var qty: Int = Int(cart_qty)!
        
        if qty > 0{
            qty -= 1
        }
        cart_count_detail()
        update_cart(qty: qty)
        change_cart_style()
       
    }
    
    func update_cart(qty: Int){
        cart_qty = String(qty)
        let cart_product = CartModel()
        cart_product.product_id = product_detail.id!
        cart_product.product_name = product_detail.package_title!
        cart_product.product_image = product_detail.get_slider_image(img_url: product_detail.slider_image1)
        cart_product.quantity = qty

        if qty > 0{
            CartModel.save_cart(cart: cart_product)
            let cart_count = CartModel.cart_count()
            lbl_count.text = cart_count
            lbl_cart.text = cart_count
        }else{
            CartModel.remove_cart(product_id: cart_product.product_id)
            
        }

        tv.reloadRows(at: [IndexPath.init(row: 2, section: 0)], with: .automatic)
        change_cart_style()
        cart_count_detail()
    }
    
    func change_cart_style(){
        let cart_count = CartModel.cart_count()
        if cart_count.elementsEqual("0"){
            view_order.backgroundColor = UIColor(named: "App Color")
            img_cart.alpha = 0
            lbl_count.alpha = 0
            img_arrow.tintColor = .black
            lbl_order.textColor = .black
            lbl_cart.alpha = 0
        }else{
            view_order.backgroundColor = UIColor.init(hexString: "#00022e")
            img_cart.alpha = 1
            lbl_count.alpha = 1
            lbl_cart.alpha = 1
            lbl_order.textColor = UIColor(named: "App Color")
            img_arrow.tintColor = .white
            lbl_count.text = cart_count
            lbl_cart.text = cart_count
       }
        
        
 }
}

extension ProductDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return detail_items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item: ProductItem = detail_items[indexPath.row]
        
        switch item.type {
        case .PRODUCT_IAMGES:
            let cell = tableView.dequeueReusableCell(withIdentifier: images_row_item, for: indexPath) as! ProductImagesRowItem
            cell.lbl_title.text = product_detail.package_title
            cell.load_images(product_detail.get_slider_image(img_url: product_detail.slider_image1), product_detail.get_slider_image(img_url: product_detail.slider_image2), product_detail.get_slider_image(img_url: product_detail.slider_image3))
            
            let first_img_gesture = ImageSelectGesture(target: self, action: #selector(select_image))
            first_img_gesture.img_url = product_detail.get_slider_image(img_url: product_detail.slider_image1)
            cell.img_first.addGestureRecognizer(first_img_gesture)
            
            let second_img_gesture = ImageSelectGesture(target: self, action: #selector(select_image))
            second_img_gesture.img_url = product_detail.get_slider_image(img_url: product_detail.slider_image2)
            cell.img_second.addGestureRecognizer(second_img_gesture)
            
            let third_img_gesture = ImageSelectGesture(target: self, action: #selector(select_image))
            third_img_gesture.img_url = product_detail.get_slider_image(img_url: product_detail.slider_image3)
            cell.img_third.addGestureRecognizer(third_img_gesture)
            return cell
        case .IMAGE_PREVIEW:
            let cell = tableView.dequeueReusableCell(withIdentifier: image_preview_row_item, for: indexPath) as! ProductImagePreviewRowItem
            cell.load_image(sel_img_url)
            let zoom_gesture = UITapGestureRecognizer(target: self, action: #selector(zoom))
            cell.view_zoom.addGestureRecognizer(zoom_gesture)
            return cell
        case .CART:
            let cell = tableView.dequeueReusableCell(withIdentifier: cart_row_item, for: indexPath) as! ProductCartRowItem
            cell.view_plus.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(add_cart)))
            cell.view_minus.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(remove_cart)))
            cell.lbl_qty.text = cart_qty

            return cell
        case .INFO:
            let cell = tableView.dequeueReusableCell(withIdentifier: info_row_item, for: indexPath) as! ProductInfoRowItem
            cell.lbl_product_size.text = product_detail.product_size
            cell.lbl_product_thickness.text = product_detail.product_thick
            return cell
        case .LAYER:
            let cell = tableView.dequeueReusableCell(withIdentifier: layer_row_item, for: indexPath) as! ProductLayerRowItem
            cell.load_info(product_detail.get_layer(category_type: category_type))
            return cell
        case .ADHESIVE:
            let cell = tableView.dequeueReusableCell(withIdentifier: adhesive_row_item, for: indexPath) as! ProductAdhesiveRowItem
            return cell
        case .FEATURE:
            let cell = tableView.dequeueReusableCell(withIdentifier: feature_row_item, for: indexPath) as! ProductFeatureRow
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.load_features(product_detail.get_feature(category_type: category_type))
            cell.cv_height.constant = cell.cv.collectionViewLayout.collectionViewContentSize.height
            return cell
        case .none:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item: ProductItem = detail_items[indexPath.row]
        
        switch item.type {
        case .PRODUCT_IAMGES:
            return 165.0
        case .IMAGE_PREVIEW:
            return 200.0
        case .CART:
            return 75.0
        case .INFO:
            return 150.0
        case .LAYER:
            return 140.0
        case .ADHESIVE:
            return 120.0
        case .none, .FEATURE:
            return UITableView.automaticDimension
        }
    }
    
    @IBAction func select_image(_ sender: ImageSelectGesture){
        sel_img_url = sender.img_url
        tv.reloadRows(at: [IndexPath.init(row: 1, section: 0)], with: .automatic)
    }
}

class ImageSelectGesture: UITapGestureRecognizer{
    var img_url: String!
}

enum ProductItemType{
    case PRODUCT_IAMGES, IMAGE_PREVIEW, CART, INFO, LAYER, ADHESIVE, FEATURE
}

struct ProductItem{
    var type : ProductItemType!
    
    init(type : ProductItemType!) {
        self.type = type
    }
}



