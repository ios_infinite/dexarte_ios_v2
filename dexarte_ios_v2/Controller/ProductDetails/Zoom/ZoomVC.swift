//
//  ZoomVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/30/21.
//

import UIKit
import SDWebImage

class ZoomVC: UIViewController {
    
  
    
    @IBOutlet weak var img_zoom: UIImageView!
     @IBOutlet weak var view_cancel: UIView!
    
    var img_icon : String!
    var pinch_gesture = UIPinchGestureRecognizer()
      
    override func viewDidLoad() {
        super.viewDidLoad()

        img_zoom.isUserInteractionEnabled = true
        img_zoom.sd_setImage(with: URL(string: img_icon), placeholderImage: UIImage(named: "ic_white"))
        print(img_icon)
        
       
        view.layoutIfNeeded()
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_cancel.addGestureRecognizer(back_gesture)

        let pinch_gesture = UIPinchGestureRecognizer(target: self, action: #selector(zoom_img))
        img_zoom.addGestureRecognizer(pinch_gesture)

    }

    @IBAction func zoom_img( _ sender : UIPinchGestureRecognizer){
        print("img gesture")
        sender.view?.transform = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale))!
        sender.scale = 1.0
    }
    
    
 
    

    @IBAction func back() {
        dismiss(animated: true, completion: nil)
    }
}
