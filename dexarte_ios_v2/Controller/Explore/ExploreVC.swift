//
//  ExploreVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/5/21.
//

import UIKit
import ObjectMapper
import Alamofire
import SDWebImage

class ExploreVC: UIViewController
                 ,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout
{
    
    @IBOutlet weak var lbl_explore_heading: UILabel!
    @IBOutlet weak var cv_product: UICollectionView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var view_bg: UIViewX!
    @IBOutlet weak var lbl_product_list: UILabel!
    
    var product_item : [ProductResponse] = []
    var cat_id: String!
    var cat_icon: String!
    var cat_title: String!
    let explore_page_row_identifier = "ExploreProductPageItem"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cv_product.dataSource   =  self
        cv_product.delegate     =  self
        
        let page_row_nib = UINib(nibName: explore_page_row_identifier, bundle: nil)
        cv_product.register(page_row_nib, forCellWithReuseIdentifier: explore_page_row_identifier)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(explore_menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
        lbl_explore_heading.set_font(font_type: .ENCORPADA, font_size: 25.0)
        lbl_explore_heading.update_space(line_space: 1.0, letter_space: 5.0)
        
        lbl_title.text = cat_title
        img_icon.image = UIImage(named: cat_icon)
        img_icon.tintColor = .gray
        
        product()
    }
    
    @IBAction func back() {
//        let vc = WelcomeVC(nibName: nil, bundle: nil)
//          panel?.center(vc)
        dismiss(animated: true, completion: nil)
    }
    
    
    
    func product() {
        let param : [String  : String] = ["pkg_cat_id" : cat_id]
        
        ProductRequest.call_request(param: param) { [self]
            (res) in
            print("cat_id \(cat_id!)")
            let product_response: [ProductResponse] = Mapper<ProductResponse>().mapArray(JSONString: res)!
            product_item = product_response
            cv_product.reloadData()
            lbl_product_list.text = "\(String(product_response.count)) Products"
        }
    }
    
    @IBAction func explore_menu() {
        
        panel?.openLeft(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return product_item.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: explore_page_row_identifier , for: indexPath) as! ExploreProductPageItem
        
        let product : ProductResponse = product_item[indexPath.item]
        
        cell.img_product.sd_setImage(with: URL(string: product.get_slider_image(img_url: product.slider_image1) ), placeholderImage: UIImage(named: "ic_white"))
        cell.lbl_product_title.text = product.package_title
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let product : ProductResponse = product_item[indexPath.item]
        let vc = ProductDetailsVC(nibName: nil, bundle: nil)
        vc.product_id = product.id
        vc.category_type = cat_title.lowercased()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2.0 , height: 150)
    }
    
}
