//
//  TrackVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/22/21.
//

import UIKit
import ObjectMapper

class TrackVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var view_back: UIView!
    
    @IBOutlet weak var lbl_heading: UILabel!
    
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var tv: UITableView!
    
    
    let track_row_identifier = "TrackRowItem"
    
    var order_no : String!
    var invoice_no : String!
    var invoice_date : String!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tv.dataSource = self
        tv.delegate =  self
       
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
        
        let track_row_nib = UINib(nibName: track_row_identifier, bundle: nil)
        tv.register(track_row_nib, forCellReuseIdentifier: track_row_identifier)
        
        
        lbl_heading.set_font(font_type: .ENCORPADA, font_size: 25.0)
        lbl_heading.update_space(line_space: 1.0, letter_space: 5.0)
       
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
    }


    
    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back (){
//        let vc = WelcomeVC(nibName: nil, bundle: nil)
//          panel?.center(vc)
        dismiss(animated: true, completion: nil)
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: track_row_identifier, for: indexPath) as! TrackRowItem
        
        cell.lbl_order_no.text = String(format: "#%@", order_no)
        cell.lbl_invoice_no.text = String(format: "#%@", invoice_no)
        cell.lbl_invoice_date.text = invoice_date
        
        let invoice_gesture = InvoiceCustomUrlGesture(target: self, action: #selector(open_url))
        invoice_gesture.url =   "https://dexarte.co.in/distributors/invoice.php?id=CLS17"
        cell.view_invoice.addGestureRecognizer(invoice_gesture)
        

        let challan_gesture = InvoiceCustomUrlGesture(target: self, action: #selector(open_url))
        challan_gesture.url =    "https://dexarte.co.in/distributors/challan.php?id=CLS17"
        cell.view_challan.addGestureRecognizer(challan_gesture)
        
        return cell
    }

    
   
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       return 500
    }
   
  
    @IBAction func open_url(_ sender : InvoiceCustomUrlGesture){
        let url: URL = URL(string: sender.url)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }

    
}


class InvoiceCustomUrlGesture : UITapGestureRecognizer{
    var url: String!
}

