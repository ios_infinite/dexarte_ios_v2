//
//  ProductVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/3/21.
//

import UIKit
import ObjectMapper

class ProductVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var cv: UICollectionView!
    @IBOutlet weak var view_explore: UIView!
    @IBOutlet weak var view_catalogue: UIView!
    @IBOutlet weak var img_icon: UIImageView!
    @IBOutlet weak var lbl_description: UILabel!
    @IBOutlet weak var img_bg: UIImageView!
    
    let icon_row_identifier = "ProductIconRowItem"
    let page_row_identifier = "ProductPageRowItem"
    
    var categories : [Category] =
        [
            
            Category(id: "1", selected: true, title: "DX", icon: "ic_dexarte", bg_name: "ic_product_dexarte", description: "<p><b>HIGH QUALITY LUXURY DECORATIVE PVC PANELS</b></p> <p>Cast an aura of class with premium decorative sheets that unfolds the brilliance of every colour.</p><ul><li>Luxurious Appearance: Makes every colour come alive. The various designs created with skilled craftsmanship provides stunning look to the environment that take your imagination to different level.</li><li>90 Degrees bendable: These sheets can be bended to 90 degrees thus giving seamless finish at the bends.</li><li>Unsurpassed Quality: Guaranteed with virgin quality base and special surface treatment.</li><li>Anti-Termite/ Anti-Borer: Safeguards the allure of original finish</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/dexarte.pdf",type: .DX ),
            Category(id: "2", selected: false, title: "DXV", icon: "ic_velvetto", bg_name: "ic_product_velvetto_dexarte", description: "<p><b>VELVET TOUCH. PERFECT MATTE. LUXE FEEL</b></p><p>Stunning colours with classy matte finish in revolutionary ANTISCRATCH technology (Barcol Hardness-4H).This incredible range gives greater creative breadth in colours with velvet touch giving sheer luxe finish.</p><ul><li>Anti-Scratch : Safeguards the allure of original finish against MAR and abrasions.</li><li>90 Degrees bendable: These sheets can be bended to 90 degrees thus eliminating visible edge lines and giving seamless finish at the bends.</li><li>Luxurious appearance with velvet feel : Makes every design come alive with soft velvet touch and luxe matte feel.</li><li>Anti Finger prints : Resistant to finger prints marks.</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/duroluxe.pdf",type: .DXV),
            Category(id: "3", selected: false, title: "DXC", icon: "ic_reflections", bg_name: "ic_reflections_dexarte", description: "<p><b>LUXURY UNICOLORS, DEEP COLOUR CORE</b></p><p>Give yourself a visual treat with smooth panels showcasing a greater depth of colors.</p><ul><li>Seamless at edges: Eliminating the edge lines and giving seamless edge finish.</li><li>Anti-Yellowing property safeguards the allure of the original finish for a long time.</li><li>Buff-able: Duroluxe REFLEXIONS is buff-able easily with regular buffing agents and tools, thus increasing the longevity.</li><li>Eco- friendly : Made from VOC free Eco friendly polymer.</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/duroluxe.pdf",type: .DXC),
            Category(id: "4", selected: false, title: "DGE", icon: "ic_ecolam", bg_name: "ic_ecolam_dexarte", description: "<p><b>HIGH QUALITY SUPER ULTRAGLOSS CRYSTAL FINISH DECORATIVE PANELS</b></p><p>Make a Luxury Statement with decorative crystal-finish panels made with eco-friendly polymers.</p><ul><li>Strong and Unbreakable: Ecolam is strong, light weight, flexible and safe from abrasions with scratch resistance -2H.</li><li>Glossy Mirror Finish: Sparkling clarity and high gloss. The surface is as shiny and clear as a back painted glass.</li><li>Anti-Yellowing : Safeguards the allure of the original finish for a long time.</li><li>Buff-able: Ecolam is buff-able easily with regular buffing agents and tools.</li><li>*ECO-Conscious: ECOLAM is made of a virgin quality recyclable polymer.It is nontoxic, odourless, environment friendly and VOC free.</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/duroglaze_ecolam.pdf",type: .DGE),
            Category(id: "5", selected: false, title: "DC", icon: "ic_crystal", bg_name: "ic_crystal_dexarte", description: "<p>With revolutionary technology of scratch resistance upto 5H, DEXIGLAS CRYSTAL non toxic panels gives a perfectly smooth, glass-like high gloss luxurious appearance to the interiors.</p><ul><li>Ultra high gloss : Dexiglas Crystal sheet exhibits glass-like qualities – clarity, brilliance, transparency.</li><li>Light Weight : It Weighs less than half that of glass. And is 10-20 times stronger than glass.</li><li>Scratch Resistant : Abrasion resistant is upto 5H.</li><li>Buff-able : Should this robust surface nonetheless become damaged, it can be re-polished without any problems with regular buffing agents and tools.</li><li>Anti-Yellowing-Anti-Fading: Anti-Yellowing property safeguards the allure of the original finish for a long time.</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/dexiglas_crystal.pdf",type: .DC),
            Category(id: "6", selected: false, title: "DA", icon: "ic_duroarte", bg_name: "ic_duroarte_dexarte", description: "<p><b>HIGH QUALITY SURFACE DECORATIVE PANELS</b></p><p >Guaranteed to cast a spell with its alluring beauty, these surface decorative panels are made of mixture of high level standard Polystyrene and Charcoal.</p><ul><li>Luxurious Copyright patented designs.</li><li>Anti-Termite / Anti-Borer property : Safegaurds the allure of the original finish with anti-termite and anti-borer Property.</li><li>Water Resistant : Insulated from getting affected by water.</li><li>Versatile nature : Can be used on walls as well as ceilings with great ease.</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/duroaret.pdf",type: .DA),
            Category(id: "7", selected: false, title: "LL", icon: "ic_luxeline (1)", bg_name: "ic_luxeline", description: "<p><b>ULTRA PREMIUM LUXURY PANELS</b></p><ul><li>Ultra high gloss : LUXELINE sheets exhibits glass-like qualities – clarity, brilliance.</li><li>Scratch Resistant : Abrasion resistant is upto 5H.</li><li>Buff-able : Should this robust surface nonetheless become damaged, it can be re-polished with regular buffing agents and tools.</li><li>Luxurious AESTHETIC look.</li></ul>",pdf: "https://dexarte.co.in/image/ecatalogue/luxeline.pdf",type: .LL),
            Category(id: "8", selected: false, title: "ALT", icon: "ic_logo_altura", bg_name: "ic_altura", description: "<p><b>Redefining the use of planks in interior as well as exterior cladding of walls &amp; ceilings.</b></p><p><b>Altura planks are made from high quality virgin grade polystyrene</b></p><ul><li>Anti-Termite / Anti-Borer property : Safeguards the allure of the original finish with anti-termite and anti-borer Property.</li><li>Water Resistant : Insulated from getting affected by water.</li><li>Versatile nature : Can be used on walls as well as ceilings with great ease.</li></ul>",pdf: "https://dexarte.co.in/product_category.php?id=9#",type: .ALT),
            Category(id: "9", selected: false, title: "ES", icon: "ic_essenczaa-logo", bg_name: "ic_Essenczaa", description: "<p><b>LACQUERED ADVENT LAMINATES</b></p><p>Raising the bar once again with the new range of acrylic high gloss glass laminates, ESSENCZAA acrylic Laminates is widely used in furniture design.</p><ul><li>Treated with a special lacquer coating on the surface these 6H anti scratch Acrylic Laminates can bear daily wear tear.</li><li>Specially treated to ensure they remain termite and borer free for years</li><li>Anti-Fading: Anti-Yellowing property safeguards the allure of the original finish for a long time.</li><li>High gloss glass like finish: Makes it an ideal choice for kitchens cabinets, bathroom and living room furniture.</li></ul>",pdf: "https://dexarte.co.in/product_category.php?id=10#",type: .ES),
            Category(id: "10", selected: false, title: "OT", icon: "ic_ornate-logo", bg_name: "ic_ornate", description: "<p><b>High Quality Stainless Steel Luxe Profiles.</b></p><ul><li>Wide range of sizes and color</li><li>These steel profiles are PVD coated through the process of electrolysis that makes it durable and long lasting.</li><li>Thus giving a shine and lustre that does not fade over several years.</li></ul>",pdf: "",type: .OT)
            
            
        ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cv.dataSource = self
        cv.delegate = self
        
        let row_nib = UINib(nibName: icon_row_identifier, bundle: nil)
        cv.register(row_nib, forCellWithReuseIdentifier: icon_row_identifier)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 25.0)
        lbl_title.update_space(line_space: 0, letter_space: 3.0)
        
        load_details(pos: 0)
        
        cv.reloadData()
    }
    
    @IBAction func menu() {
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back() {
        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
//        dismiss(animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  categories.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: icon_row_identifier , for: indexPath) as! ProductIconRowItem
        
        let category : Category = categories[indexPath.item]
        
        cell.lbl_icon.text = category.title
        
        
        if category.selected {
            cell.view_bg.backgroundColor = .white
            cell.lbl_icon.textColor = UIColor(named: "App Color")
            
        }else{
            cell.view_bg.backgroundColor = .clear
            cell.lbl_icon.textColor = .systemGray
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 3, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        change_icon_style(pos: indexPath.item)
        
        load_details(pos: indexPath.item)
        
        cv.reloadData()
        
    }
    
    func load_details(pos: Int){
        let category : Category =  categories[pos]
        
        img_bg.image = UIImage(named: category.bg_name)
        img_icon.image = UIImage(named: category.icon)
        img_icon.tintColor = UIColor(named: "Font Color")
        lbl_description.attributedText = category.description.htmlToAttributedString
        lbl_description.textColor = UIColor(named: "Font Color")
        
        let url_gesture = OpenUrlGesture(target: self, action: #selector(open_url))
        url_gesture.url = category.pdf
        view_catalogue.addGestureRecognizer(url_gesture)
        
        let view_button_gesture = ProductViewButtonGesture(target: self, action: #selector(redirect_vc))
        view_button_gesture.index = pos
        view_explore.addGestureRecognizer(view_button_gesture)
    }
    
    @IBAction func redirect_vc(_ sender : ProductViewButtonGesture){
        let category: Category = categories[sender.index]
        
        let vc = ExploreVC(nibName: nil, bundle: nil)
        vc.cat_id = category.id
        vc.cat_icon = category.icon
        vc.cat_title = category.title
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
    
    
    @IBAction func open_url(_ sender : OpenUrlGesture){
        let url =  URL(string: sender.url)
        UIApplication.shared.open(url!,options: [:], completionHandler: nil)
    }
    
    func change_icon_style(pos: Int){
        for i in 0..<categories.count{
            if pos == i{
                categories[i].selected =  true
            }else{
                categories[i].selected = false
            }
        }
        
        cv.reloadData()
    }
    
    
}

class OpenUrlGesture : UITapGestureRecognizer{
    var url : String!
}

class ProductViewButtonGesture: UITapGestureRecognizer{
    var index: Int!
}

struct Category {
    
    enum ProductType {
        case DX, DXV, DXC, DGE, DC,DA,LL,ALT,ES,OT
    }
    
    var id : String!
    var selected : Bool!
    var title : String!
    var icon : String!
    var bg_name : String!
    var description : String!
    var pdf : String!
    var type : ProductType
    
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
