//
//  ReviewVC.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 30/06/21.
//

import UIKit

class ReviewVC: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var cv_icon: UICollectionView!
    @IBOutlet weak var cv_page: UICollectionView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var view_menu: UIView!
    
    let review_row_identifier = "ReviewIconRowItem"
    let read_row_identifier = "ReadIconRowItem"
    let message_row_identifier = "MessageIconRowItem"
    let email_row_identifier = "EmailIconRowItem"
    
    var review_icon : [ReviewIconItem] =
        
        [
        ReviewIconItem(icon: UIImage(named: "ic_book"), selected: true),
        ReviewIconItem(icon: UIImage(named: "ic_review_email"), selected: false),
        ReviewIconItem(icon: UIImage(named: "ic_messenger"), selected: false)
        
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 20.0)
        lbl_title.update_space(line_space: 3.0, letter_space: 2.0)
  
        cv_icon.dataSource = self
        cv_icon.delegate = self
        
        cv_page.dataSource = self
        cv_page.delegate = self
        
    let icon_row_nib = UINib(nibName: review_row_identifier, bundle: nil )
        cv_icon.register(icon_row_nib, forCellWithReuseIdentifier: review_row_identifier)
        
        let read_row_nib = UINib(nibName: read_row_identifier, bundle: nil )
            cv_page.register(read_row_nib, forCellWithReuseIdentifier: read_row_identifier)
        
        let message_row_nib = UINib(nibName: message_row_identifier, bundle: nil )
            cv_page.register(message_row_nib, forCellWithReuseIdentifier: message_row_identifier)
        
        let email_row_nib = UINib(nibName: email_row_identifier, bundle: nil )
            cv_page.register(email_row_nib, forCellWithReuseIdentifier: email_row_identifier)
        
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
       
    }
    
    @IBAction func menu(){
        panel?.openLeft(animated: true)
    }
    
    @IBAction func back (){
       
        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return review_icon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        
        if collectionView == cv_icon{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: review_row_identifier, for: indexPath) as! ReviewIconRowItem
            
            let review_item : ReviewIconItem = review_icon[indexPath.item]
            cell.img_icon.image = review_item.icon
            if review_item.selected {
            cell.view_bg.backgroundColor = .white
            cell.img_icon.tintColor = UIColor(named: "App Color")
            return cell
        }else{
            
            cell.view_bg.backgroundColor = .clear
            cell.img_icon.tintColor = .gray
        }
            return cell
        
        }else{
            
            if indexPath.item == 0 {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: read_row_identifier, for: indexPath) as! ReadIconRowItem
                let pdf_gesture = CustomUrlGesture(target: self, action: #selector(open_url))
                pdf_gesture.url = "https://www.dexarte.co.in/image/newsFeedSingle/dexarte_news.pdf"
                cell.view_read.addGestureRecognizer(pdf_gesture)
                
                return cell
            
            
            }else if indexPath.item == 1{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: email_row_identifier, for: indexPath) as! EmailIconRowItem
                
                return cell
            }else{
                
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: message_row_identifier, for: indexPath) as! MessageIconRowItem
                
                return cell
                
            }
        }
    }
    
    @IBAction func open_url(_ sender : CustomUrlGesture){
        let url: URL = URL(string: sender.url)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    class CustomUrlGesture : UITapGestureRecognizer{
        var url: String!
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == cv_page{
            let pageWidth = cv_page.frame.size.width
            let page_number: Int = Int(self.cv_page.contentOffset.x / pageWidth)
            print("current page ", page_number)
            change_icon_style(pos: page_number)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        change_icon_style(pos: indexPath.item)
        cv_page.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func change_icon_style(pos: Int){
        for i in 0..<review_icon.count{
            if pos == i{
                review_icon[i].selected =  true
            }else{
                review_icon[i].selected = false
            }
        }
        
        cv_icon.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == cv_icon{
            return CGSize(width: collectionView.frame.size.width / 3, height: 100)
        }else{
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
    }
    
}
 
struct ReviewIconItem{
    
    var icon       : UIImage!
    var selected   : Bool!
    
}

