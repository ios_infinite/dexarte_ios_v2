//
//  AboutVC.swift
//  dexarte_ios_v2
//
//  Created by MAC 2 on 30/06/21.
//

import UIKit

class AboutVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var lbl_title: UILabel!
    @IBOutlet weak var cv_icon: UICollectionView!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var cv_page: UICollectionView!
    @IBOutlet weak var view_menu: UIView!
    
    let icon_row_identifier = "AboutIconRowItem"
    let flower_row_identifier = "AboutFlowerRowItem"
    let trophy_row_identifier = "AboutTrophyRowItem"
    let pdf_row_identifier =  "AboutPdfRowItem"
    
    var pageControl : String!
    
    var icon : [IconItem] =
        
        [
            IconItem(icon: UIImage(named:"ic_Dexarte_Gray"), selected: true),
            IconItem(icon: UIImage(named:"ic_trophy"), selected: false),
            IconItem(icon: UIImage(named:"ic_pdf"), selected: false)
            
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cv_icon.dataSource = self
        cv_icon.delegate = self
        
        cv_page.dataSource = self
        cv_page.delegate = self
        
        lbl_title.set_font(font_type: .ENCORPADA, font_size: 20)
        lbl_title.update_space(line_space: 5.0, letter_space: 2.0)
        
        let icon_row_nib = UINib(nibName: icon_row_identifier, bundle: nil)
        cv_icon.register(icon_row_nib, forCellWithReuseIdentifier: icon_row_identifier)
        
        let flower_row_nib = UINib(nibName: flower_row_identifier, bundle: nil)
        cv_page.register(flower_row_nib, forCellWithReuseIdentifier: flower_row_identifier)
        
        let trophy_row_nib = UINib(nibName: trophy_row_identifier, bundle: nil)
        cv_page.register(trophy_row_nib, forCellWithReuseIdentifier: trophy_row_identifier)
        
        let pdf_row_nib = UINib(nibName: pdf_row_identifier, bundle: nil)
        cv_page.register(pdf_row_nib, forCellWithReuseIdentifier: pdf_row_identifier)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
    }
    
    
    @IBAction func menu(){

        panel?.openLeft(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return icon.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == cv_icon{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: icon_row_identifier, for: indexPath) as! AboutIconRowItem
            
            let icon_item : IconItem = icon[indexPath.item]
            cell.img_icon.image = icon_item.icon
            if icon_item.selected{
                cell.view_bg.backgroundColor = .white
                cell.img_icon.tintColor = UIColor(named: "App Color")
                
            }else{
                cell.view_bg.backgroundColor = .clear
                cell.img_icon.tintColor = .systemGray
                
            }
            return cell
        }else {
            if indexPath.item == 0{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: flower_row_identifier, for: indexPath) as! AboutFlowerRowItem
                return cell
            }else if indexPath.item == 1{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: trophy_row_identifier, for: indexPath) as! AboutTrophyRowItem
                return cell
                
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: pdf_row_identifier, for: indexPath) as! AboutPdfRowItem
                
                let pdf_gesture = CustomUrlGesture(target: self, action: #selector(open_url))
                pdf_gesture.url = "https://www.dexarte.co.in/image/about/dexartBrochure.pdf"
                cell.view_pdf.addGestureRecognizer(pdf_gesture)
                
                return cell
            }
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == cv_page{
            let pageWidth = cv_page.frame.size.width
            let page_number: Int = Int(self.cv_page.contentOffset.x / pageWidth)
            print("current page ", page_number)
            change_icon_style(pos: page_number)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if  collectionView == cv_icon{
            return CGSize(width: collectionView.frame.size.width / 3, height: 100.0)
        }else{
            return CGSize(width: collectionView.frame.size.width, height: collectionView.frame.size.height)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        change_icon_style(pos: indexPath.item)
        cv_page.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func change_icon_style(pos: Int){
        for i in 0..<icon.count{
            if pos == i{
                icon[i].selected =  true
            }else{
                icon[i].selected = false
            }
        }
        
        cv_icon.reloadData()
    }
    
    @IBAction func open_url(_ sender : CustomUrlGesture){
        let url: URL = URL(string: sender.url)!
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    class CustomUrlGesture : UITapGestureRecognizer{
        var url: String!
    }
    
    @IBAction func back(){
      let vc = WelcomeVC(nibName: nil, bundle: nil)
        panel?.center(vc)
        //vc.modalPresentationStyle = .fullScreen
        //present(vc, animated: true, completion: nil)
//        dismiss(animated: true, completion: nil)
    }
}



struct IconItem {
    
    var icon      : UIImage!
    var selected  : Bool!
}
