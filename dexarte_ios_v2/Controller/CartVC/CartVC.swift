

//
//  CartVC.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/23/21.
//

import UIKit
import SDWebImage
import ObjectMapper


class CartVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var view_menu: UIView!
    @IBOutlet weak var view_cart: UIView!
    @IBOutlet weak var tv: UITableView!
    @IBOutlet weak var lbl_cart: UILabel!
    @IBOutlet weak var view_back: UIView!
    @IBOutlet weak var view_checkout: UIViewX!
    
    var icon : String!
    var product_id : String!
    var product_name : String!
    var product_image : String!
    var cart_product : [CartModel]!
    let row_identifier = "CartRowItem"
    var login_response : LoginResponse!

    
    var cart_count : String = "0"
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if PreferencesUtil.checkPrefs(key: Constants.LOGIN_RESPONSE) == true{
            let str_login_response = PreferencesUtil.getFromPrefs(key: Constants.LOGIN_RESPONSE)
            login_response = Mapper<LoginResponse>().map(JSONString: str_login_response)
        }
               
        
        view_cart.alpha = 0
        view_checkout.alpha = 0
         
        tv.dataSource = self
        tv.delegate = self
        
        lbl_cart.set_font(font_type: .ENCORPADA, font_size: 25)
        
        tv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 85, right: 0)
        
        let row_nib = UINib(nibName: row_identifier, bundle: nil)
        tv.register(row_nib, forCellReuseIdentifier: row_identifier)
        
        let back_gesture = UITapGestureRecognizer(target: self, action: #selector(back))
        view_back.addGestureRecognizer(back_gesture)
        
        let menu_gesture = UITapGestureRecognizer(target: self, action: #selector(menu))
        view_menu.addGestureRecognizer(menu_gesture)
        
        let order_gesture = UITapGestureRecognizer(target: self, action: #selector(order))
        view_checkout.addGestureRecognizer(order_gesture)
        
        cart_product = CartModel.cart_list()
        
        if cart_product.count > 0{
            view_cart.alpha = 0
            view_checkout.alpha = 1
        }else{
            view_cart.alpha = 1
            view_checkout.alpha = 0
        }
        
        tv.reloadData()
    }
    
    
    @IBAction func menu() {
        panel?.openLeft(animated: true)
    }
  
    @IBAction func back() {

        let vc = WelcomeVC(nibName: nil, bundle: nil)
          panel?.center(vc)
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cart_product.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: row_identifier, for: indexPath) as! CartRowItem
        let cart_img : CartModel = cart_product[indexPath.row]
 
        cell.img_icon.sd_setImage(with: URL(string: cart_img.product_image)!, placeholderImage: UIImage(named: "ic_white"))
        print(URL(string: cart_img.get_slider_image(img_url: cart_img.product_image)))
        let cart : CartModel = cart_product[indexPath.row]
        cell.lbl_id.text = cart.product_name
        cell.lbl_count.text = String(cart.quantity)
        
        let delete_gesture = CartGesture(target: self, action: #selector(delete_cart))
        delete_gesture.id = cart.product_id
        cell.view_delete.addGestureRecognizer(delete_gesture)
        
        
        let add_gesture  = CartGesture(target: self, action: #selector(add_cart))
        add_gesture.count = cell.lbl_count
        cell.view_add.addGestureRecognizer(add_gesture)
        
        let minus_gesture  = CartGesture(target: self, action: #selector(minus_cart))
        minus_gesture.count = cell.lbl_count
        minus_gesture.id = cart.product_id
        cell.view_minus.addGestureRecognizer(minus_gesture)
        
        return cell
    }
    
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    @IBAction func delete_cart(_ sender : CartGesture) {
        CartModel.remove_cart(product_id: sender.id)
        cart_product = CartModel.cart_list()
        if cart_product.count > 0{
            view_cart.alpha = 0
            view_checkout.alpha = 1
        }else{
            view_cart.alpha = 1
            view_checkout.alpha = 0
        }
        tv.reloadData()
    }
   
    @IBAction func add_cart(_ sender : CartGesture) {
        var qty : Int = Int(sender.count.text!)!
        if qty >= 0 {
            qty+=1
            sender.count.text = String(qty)
 }
        update_cart(qty: qty)

     }
    
    @IBAction func minus_cart(_ sender : CartGesture) {
        var qty : Int = Int(sender.count.text!)!
        if qty > 0 {
            qty -= 1
            sender.count.text = String(qty)
        }
         update_cart(qty: qty)
 }
    
    
    func update_cart(qty: Int){
        cart_count = String(qty)
        let cart_products = CartModel()
        cart_products.product_id = product_id!
        cart_products.product_name = product_name!

        cart_products.product_image = product_image!

        cart_products.quantity = qty
        if cart_products.quantity > 0{
            print(cart_products.quantity)
            CartModel.save_cart(cart: cart_products)
            var cart_count = CartModel.cart_count()
   
        }else{
            CartModel.remove_cart(product_id: cart_products.product_id)
            cart_product = CartModel.cart_list()
            if cart_product.count > 0{
                view_cart.alpha = 0
                view_checkout.alpha = 1
            }else{
                view_cart.alpha = 1
                view_checkout.alpha = 0
            }
            tv.reloadData()
        }
    }
    
    
    @IBAction func order() {

       // if login_response!==nil {
        if PreferencesUtil.checkPrefs(key: Constants.LOGIN_RESPONSE) == true{
            place_order()
        }else{
            let vc = LoginVC(nibName: nil, bundle: nil)
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120.0
    }
    
    
    func place_order() {
        
        var productList : [ProductList] = []
                var placeOrderModel : PlaceOrderModel!

        let cart_product = CartModel.cart_list()

        for i in 0...cart_product.count-1{
            
            let productData = ProductList()
            productData.product_id = cart_product[i].product_id
            productData.qty = String(cart_product[i].quantity)
            productData.email = login_response.detail.email
                    
            productList.append(productData)
            print("item1\(productList)")

        }
            
        placeOrderModel = PlaceOrderModel.init(pro: productList)
                
                let json_str = placeOrderModel.toJSONString()!
                print("item2\(json_str)")
        
        
        PlaceOrderRequest.call_request(param: json_str){
            (res) in
            print(res)
            let order_response : PlaceOrderResponse = Mapper<PlaceOrderResponse>().map(JSONString: res)!
            
            
//            let cart_products = CartModel()
            if order_response.status.elementsEqual("1") {
                CartModel.delete_cart()
//                let cart_products = CartModel()
               
                
//                let vc = ProductDetailsVC(nibName: nil, bundle: nil)
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
                //self.dismiss(animated: true, completion: nil)

                
//                self.presentingViewController?.dismiss(animated: false, completion: {
//
//                    self.view.makeToast(order_response.response, duration: 0.5)
//                    print(order_response.response!)
//                    let vc = OrderVC(nibName: nil, bundle: nil)
//                    vc.modalPresentationStyle = .fullScreen
//                    self.present(vc, animated: true, completion: nil)
//
//                })

                
                    self.view.makeToast(order_response.response, duration: 0.5)
                    print(order_response.response!)
                    let vc = OrderVC(nibName: nil, bundle: nil)
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)

                
            }else{
                
                self.view.makeToast(order_response.response, duration: 0.5)
//                print(order_response.response!)
//                let vc = OrderVC(nibName: nil, bundle: nil)
//                vc.modalPresentationStyle = .fullScreen
//                self.present(vc, animated: true, completion: nil)
            }
            
        }
        
    }
}


class CartGesture : UITapGestureRecognizer{
    var id : String!
    var count : UILabel!
}

