//
//  PlaceOrderModel.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/23/21.
//

import Foundation
import  ObjectMapper

class PlaceOrderModel : Mappable{
    
    var product : [ProductList]!
    
    
    init(pro : [ProductList]) {
        product = pro
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        product <- map["products"]
    }
    
}


class ProductList : Mappable {
    
    var product_id : String!
    var qty : String!
    var email : String!
    
    init() {
    }
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        product_id  <- map["product_id"]
        qty         <- map["quantity"]
        email       <- map["email"]
    }
    
}
