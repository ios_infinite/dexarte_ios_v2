//
//  OrderResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/1/21.
//

import Foundation

import ObjectMapper


class OrderResponse : Mappable{
    
    var id                  :String!
    var date_created        :String!
    var order_number        :String!
    var produduct_id        :String!
    var quantity            :String!
    var email               :String!
    var number_of_quantity  :String!
    var request_qty         :String!
    var status              :String!
    var approved            :String!
    var send_status         :String!
    
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id                      <- map["id"]
        date_created            <- map["date_created"]
        order_number            <- map["order_number"]
        produduct_id            <- map["product_id"]
        quantity                <- map["quantity"]
        email                   <- map["email"]
        number_of_quantity      <- map["number_of_qty"]
        request_qty             <- map["request_qty"]
        status                  <- map["status"]
        approved                <- map["approved"]
        send_status             <- map["send_status"]
        
        
    }
    
}
