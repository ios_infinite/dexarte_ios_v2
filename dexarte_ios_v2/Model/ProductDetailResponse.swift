//
//  ProductDetailResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/16/21.
//

import Foundation
import ObjectMapper

class ProductDetailResponse : Mappable{
    
    
    var id              : String!
    var pkg_cat_id      : String!
    var date_created    : String!
    var date_updated    : String!
    var package_title   : String!
    var product_size    : String!
    var product_thick   : String!
    var slider_image2   : String!
    var slider_image3   : String!
    var slider_image4   : String!
    var slider_desc1    : String!
    var slider_image1   : String!
    var number_of_qty   : String!
    var units           : String!
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id                  <- map["id"]
        pkg_cat_id          <- map["pkg_cat_id"]
        date_created        <- map["date_created"]
        date_updated        <- map["date_updated"]
        package_title       <- map["package_title"]
        product_size        <- map["product_size"]
        product_thick       <- map["product_thick"]
        slider_image2       <- map["slider_image2"]
        slider_image3       <- map["slider_image3"]
        slider_image4       <- map["slider_image4"]
        slider_desc1        <- map["slider_desc1"]
        slider_image1       <- map["slider_image1"]
        number_of_qty       <- map["number_of_qty"]
        units               <- map["units"]
    }
    
    func get_slider_image(img_url: String) -> String{
        return String(format: "https://www.dexarte.co.in/admin/uploads/package/%@", img_url)
    }
    
    
    
    func get_layer(category_type: String) -> ProductLayer{
        
        switch category_type {
        case "dx":
            return ProductLayer(layer_count: 3, image: "ic_dx_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "High Quality Prime Polymer film of designs", "Premium Quality PVC Base"])
        case "dxv":
            return ProductLayer(layer_count: 3, image: "ic_dxv_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "High Quality Prime Polymer film of designs with Anti-Scratch.Super Matt & Velvet Touch Finish that has a High Abrasion Resistance", "Premium Quality PVC Base"])
        case "dxc":
            return ProductLayer(layer_count: 2, image: "ic_dxc_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "", "Premium Polymer of Virgin Quality that has High Gloss Deep Colour & Seamless Edges"])
        case "dge":
            return ProductLayer(layer_count: 3, image: "ic_dge_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "Eco-Friendly Polymer with Glass Look & Abrasion Resistant", "High grade Polymer with Design & Colours"])
        case "dc":
            return ProductLayer(layer_count: 3, image: "ic_dc_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "High gloss and abrasion resistant Premium Virgin quality Acrylic", "High grade Polymer with Design & Colours"])
        case "da":
            return ProductLayer(layer_count: 2, image: "ic_da_layer", description: ["Luxurious Design Color Foil.", "", "Textured High Grade Virgin Quality Polystyrene Base.Textured High Grade Virgin Quality Polystyrene Base."])
        case "ll":
            return ProductLayer(layer_count: 3, image: "ic_ll_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "High gloss and abrasion resistant Premium Virgin quality Acrylic", "High grade Polymer with Design & Colours"])
        case "alt":
            return ProductLayer(layer_count: 2, image: "ic_alt_layer", description: ["Luxurious Design Color Foil.", "", "Textured High Grade Virgin Quality Polystyrene Base."])
        case "es":
            return ProductLayer(layer_count: 3, image: "ic_es_layer", description: ["PE Protective masking with the trust of Premium Brand DEXARTE Logo", "High gloss and abrasion resistant Premium Virgin quality Acrylic", "High grade Polymer with Design & Colours"])
        case "ot":
            return ProductLayer(layer_count: 0, image: "", description: ["", "", ""])
        default:
            return ProductLayer()
        }
    }
    
    func get_feature(category_type: String) -> [ProductFeature]{
        switch category_type {
        case "dx":
            return
                [
                    ProductFeature(title: "Luxury", image: "ic_feature_luxury"),
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "Easy To Clean / Zero Maintenance", image:  "ic_feature_easy_clean"),
                    ProductFeature(title: "90o Bendable", image:  "ic_feature_bendable"),
                    ProductFeature(title: "Water Proof", image: "ic_feature_water_proof"),
                    ProductFeature(title: "Anti-Termite", image:  "ic_feature_anti")
                    ]
       
        case "dxv":
            return
                [
                    ProductFeature(title: "Luxury", image: "ic_feature_luxury"),
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "Easy To Clean / Zero Maintenance", image:  "ic_feature_easy_clean"),
                    ProductFeature(title: "90o Bendable", image:  "ic_feature_bendable"),
                    ProductFeature(title: "Water Proof", image: "ic_feature_water_proof"),
                    ProductFeature(title: "Anti-Termite", image:  "ic_feature_anti"),
                    ProductFeature(title: "Scratch Resistant", image: "ic_feature_scratch"),
                    ProductFeature(title: "Anti-Fingerprint", image: "ic_feature_anti_fnger"),
                    ProductFeature(title: "Soft Touch", image: "ic_feature_soft_touch")
                   
                ]
            
        case "dxc":
            return
                [
                    ProductFeature(title: "Luxury", image: "ic_feature_luxury"),
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "Easy To Clean / Zero Maintenance", image:  "ic_feature_easy_clean"),
                    ProductFeature(title: "Water Proof", image: "ic_feature_water_proof"),
                    ProductFeature(title: "Anti-Termite", image:  "ic_feature_anti"),
                    ProductFeature(title: "Eco-Friendly/ Bio Degradeable", image: "ic_feature_eco-friendly"),
                    ProductFeature(title: "Feather Lite", image: "ic_feature_lite"),
                    ProductFeature(title: "No Fading/ No Yellowing", image: "ic_feature_nofading"),
                    ProductFeature(title: "Approved by R&D Buffing", image: "ic_feature_buffing")
                ]
        case "dge":
            return
                [
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "Eco-Friendly/ Bio Degradeable", image: "ic_feature_eco-friendly"),
                    ProductFeature(title: "Scratch Resistant", image: "ic_feature_scratch"),
                    ProductFeature(title: "Easy To Clean / Zero Maintenance", image:  "ic_feature_easy_clean"),
                    ProductFeature(title: "Eco-Friendly", image: "ic_feature_eco"),
                    ProductFeature(title: "No Fading/ No Yellowing", image: "ic_feature_nofading"),
                    ProductFeature(title: "Excellent UV Resistance", image: "ic_feature_uv"),
                    ProductFeature(title: "Approved by R&D Buffing", image: "ic_feature_buffing"),
                    ProductFeature(title: "Normal Woodworking Tool Processing", image: "ic_feature_woodwork")
                   ]
            
        case "dc":
            return
                [
                    ProductFeature(title: "Excellent UV Resistance", image: "ic_feature_uv"),
                    ProductFeature(title: "Chalk easily Writes then Wipes", image: "ic_feature_chalk"),
                    ProductFeature(title: "Scratch Resistant", image: "ic_feature_scratch"),
                    ProductFeature(title: "Normal Woodworking Tool Processing", image:  "ic_feature_woodwork"),
                    ProductFeature(title: "10x More Break resistant", image: "ic_feature_break_resistant"),
                    ProductFeature(title: "Enhanced Anti-bacterial Properties", image: "ic_feature_anti_bacterial"),
                    ProductFeature(title: "Heat and Humidity Resistant", image: "ic_feature_heat"),
                    ProductFeature(title: "Approved by R&D Buffing", image: "ic_feature_buffing"),
                    ProductFeature(title: "No Fading/ No Yellowing", image: "ic_feature_woodwork")
                    ]
            
        case "da":
            return
                [
                    ProductFeature(title: "Luxury", image: "ic_feature_luxury"),
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "Water Proof", image: "ic_feature_water_proof"),
                    ProductFeature(title: "Anti-Termite", image:  "ic_feature_anti"),
                    ProductFeature(title: "Feather Lite", image: "ic_feature_lite")
                     ]
        case "ll":
            return
                [
                    ProductFeature(title: "Excellent UV Resistance", image: "ic_feature_uv"),
                    ProductFeature(title: "Chalk easily Writes then Wipes", image: "ic_feature_chalk"),
                    ProductFeature(title: "Scratch Resistant", image: "ic_feature_scratch"),
                    ProductFeature(title: "Normal Woodworking Tool Processing", image:  "ic_feature_woodwork"),
                    ProductFeature(title: "10x More Break resistant", image: "ic_feature_break_resistant"),
                    ProductFeature(title: "Enhanced Anti-bacterial Properties", image: "ic_feature_anti_bacterial"),
                    ProductFeature(title: "Heat and Humidity Resistant", image: "ic_feature_heat"),
                    ProductFeature(title: "Approved by R&D Buffing", image: "ic_feature_buffing"),
                    ProductFeature(title: "No Fading/ No Yellowing", image: "ic_feature_woodwork")
                  ]
            
        case "alt":
            return
                [
                    ProductFeature(title: "Luxury", image: "ic_feature_luxury"),
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "Water Proof", image: "ic_feature_water_proof"),
                    ProductFeature(title: "Anti-Termite", image:  "ic_feature_anti"),
                    ProductFeature(title: "Feather Lite", image: "ic_feature_lite"),
                    ProductFeature(title: "Aesthetic Look", image: "ic_feature_luxury"),
                    ProductFeature(title: "No Formaldehyde", image: "ic_feature_nofading"),
                    ProductFeature(title: "No Voc Content", image: "ic_feature_uv")
                    ]
            
        case "es":
            return
                [
                    ProductFeature(title: "Antiscratch", image: "ic_feature_antiscratch"),
                    ProductFeature(title: "Premium Quality", image: "ic_feature_premium_quality"),
                    ProductFeature(title: "SGS", image: "ic_feature_sg"),
                    ProductFeature(title: "ROHS", image:  "ic_feature_rohs"),
                    ProductFeature(title: "Anti Yellow", image: "ic_feature_anti_yellow"),
                    ProductFeature(title: "Light Weight", image: "ic_feature_light"),
                    ProductFeature(title: "Termite & Borer Free", image: "ic_feature_termite")
                    ]
        default:
            return []
        }
    }
}

struct ProductLayer{
    var layer_count     : Int!
    var image           : String!
    var description     : [String]!
}

struct ProductFeature{
    var title   : String!
    var image   : String!
}
