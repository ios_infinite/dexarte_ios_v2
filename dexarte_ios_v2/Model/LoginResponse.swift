//
//  LoginResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/18/21.
//

import Foundation
import ObjectMapper

class LoginResponse: Mappable{

    var status      : String!
    var response    : String!
    var detail      : UserDetail!
    var error       : LoginError!

    required init?(map: Map) {
        mapping(map: map)
    }
    func mapping(map: Map) {

        status      <- map ["status"]
        response    <- map ["response"]
        detail      <- map ["user_detail"]
        error       <- map ["error"]


    }
}


class UserDetail : Mappable {

    var id                : String!
    var date_created      : String!
    var name              : String!
    var email             : String!
    var password          : String!
    var cpassword         : String!
    var gst               : String!
    var pan               : String!
    var address           : String!
    var tel               : String!
    var city              : String!
    var state             : String!
    var dispatch          : String!


    required init?(map: Map) {
        mapping(map: map)
    }

    func mapping(map: Map) {

        id                   <- map ["id"]
        date_created         <- map ["date_created"]
        name                 <- map ["name"]
        email                <- map ["email"]
        password             <- map ["password"]
        cpassword            <- map ["cpassword"]
        gst                  <- map ["gst"]
        pan                  <- map ["pan"]
        address              <- map ["address"]
        tel                  <- map ["tel"]
        city                 <- map ["city"]
        state                <- map ["state"]
        dispatch             <- map ["dispatch"]
    }


}


class LoginError : Mappable{
    
    var status          : String!
    var response        : String!
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        status          <- map["status"]
        response        <- map["response"]
    }
    
}

