//
//  CartModel.swift
//  sun_shine
//
//  Created by ananth on 06/03/21.
//

import Foundation
import ObjectMapper

class CartModel: Mappable{
    var product_id      :String!
    var product_name    :String!
    var product_image   :String!
    var quantity        :Int!
    
    init() {}
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        product_id      <- map["product_id"]
        product_name    <- map["product_name"]
        product_image   <- map["product_image"]
        quantity        <- map["quantity"]
    }
    
    
    func get_slider_image(img_url: String) -> String{
        return String(format: "https://www.dexarte.co.in/admin/uploads/package/%@", img_url)
    }
    
    static func save_cart(cart: CartModel){
        remove_cart(product_id: cart.product_id)
        var carts: [CartModel] = cart_list()
        carts.append(cart)
        PreferencesUtil.saveToPrefs(key: Constants.CART, value: carts.toJSONString()!)
        
        print("cart products \(cart_list().toJSONString()!)")
    }
    
    static func remove_cart(product_id: String){
        var carts: [CartModel] = cart_list()
        
        let prod_index = carts.firstIndex { (cart) -> Bool in
            return cart.product_id.elementsEqual(product_id)
        }
        
        if prod_index != nil{
            carts.remove(at: prod_index!)
            PreferencesUtil.saveToPrefs(key: Constants.CART, value: carts.toJSONString()!)
        }
    }
    
    static func cart_list() -> [CartModel]{
        var carts: [CartModel] = []
        if PreferencesUtil.checkPrefs(key: Constants.CART){
            let str_cart = PreferencesUtil.getFromPrefs(key: Constants.CART)
            carts  = Array<CartModel>(JSONString: str_cart)!
        }
        return carts
    }
    
    static func get_cart(product_id: String) -> CartModel{
        var cart_model = CartModel()
        let carts: [CartModel] = cart_list()
        
        let prod_index = carts.firstIndex { (cart) -> Bool in
            return cart.product_id.elementsEqual(product_id)
        }
        
        if prod_index != nil{
            cart_model = carts[prod_index!]
        }
        
        return cart_model
    }
    
    static func delete_cart(){
        let carts : [CartModel] = []
        PreferencesUtil.saveToPrefs(key: Constants.CART, value: carts.toJSONString()!)
    }
    
    static func cart_count()->String{
        let carts: [CartModel] = cart_list()
        return String(carts.count)
    }
}
