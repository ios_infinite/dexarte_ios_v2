//
//  OrderDetailResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/2/21.
//

import Foundation
import ObjectMapper



class OrderDetailResponse : Mappable{
    
    var order_no    :String!
    var order_date  :String!
    var product     :[OrderProductDetail]!
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        order_no    <- map["order_no"]
        order_date  <- map["order_date"]
        product     <- map["products"]
    }
    
}

class OrderProductDetail : Mappable {
    
    var order_number               :String!
    var date_created               :String!
    var packages_category_name     :String!
    var package_title              :String!
    var request_qty                :String!
    var status                     :String!
    var approved                   :String!
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        order_number             <- map["order_number"]
        date_created             <- map["date_created"]
        packages_category_name   <- map["packages_category_name"]
        package_title            <- map["package_title"]
        request_qty              <- map["request_qty"]
        status                   <- map["status"]
        approved                 <- map["approved"]
    }
    
}
