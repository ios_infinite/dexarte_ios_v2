//
//  PlaceOrderResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/23/21.
//

import Foundation
import  ObjectMapper


class PlaceOrderResponse :  Mappable {
    
    var status      : String!
    var response    : String!
    var order_no    : Int!
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        status       <- map["status"]
        response     <- map["response"]
        order_no     <- map["order_number"]
    }
    
}

