//
//  ProductResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 8/5/21.
//

import Foundation
import ObjectMapper


class ProductResponse : Mappable{
    
    var id               : String!
    var pkg_cat_id       : String!
    var date_created     : String!
    var date_updated     : String!
    var package_title    : String!
    var product_size     : String!
    var product_thick    : String!
    var slider_image2    : String!
    var slider_image3    : String!
    var slider_image4    : String!
    var slider_desc1     : String!
    var slider_image1    : String!
    var number_of_qty    : String!
    var units            : String!
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id                <- map ["id"]
        pkg_cat_id        <- map ["pkg_cat_id"]
        date_created      <- map ["date_created"]
        date_updated      <- map ["date_updated"]
        package_title     <- map ["package_title"]
        product_size      <- map ["product_size"]
        product_thick     <- map ["product_thick"]
        slider_image2     <- map ["slider_image2"]
        slider_image3     <- map ["slider_image3"]
        slider_image4     <- map ["slider_image4"]
        slider_desc1      <- map ["slider_desc1"]
        slider_image1     <- map ["slider_image1"]
        number_of_qty     <- map ["number_of_qty"]
        units             <- map ["units"]
    }
    
    func get_slider_image(img_url: String) -> String{
        return String(format: "https://www.dexarte.co.in/admin/uploads/package/%@", img_url)
    }
}



