//
//  InvoiceResponse.swift
//  dexarte_ios_v2
//
//  Created by Aahana Sethia on 9/21/21.
//

import Foundation
import  ObjectMapper


class InvoiceResponse : Mappable {
    
    var id                   :String!
    var date_created         :String!
    var order_number         :String!
    var invoice_company      :String!
    var invoice_number       :String!
    var challan_number       :String!
    var igst                 :String!
    var cgst                 :String!
    var sgst                 :String!
    var customer_email       :String!
    var send_quantity        :String!
    var product_id           :String!
    var dispatch_date        :String!
    var dispatch_through     :String!
    var status               :String!
    var price                :String!
    var amount               :String!
    var packing_material     :String!
    var packing              :String!
    var category_id          :String!
    var recevied_amount      :String!
    var packing_hsn          :String!
    var ir_number            :String!
    var delivery_date        :String!
    var invoice_with_tax     :Int!
    
    
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        
        id                      <- map["id"]
        date_created            <- map["date_created"]
        order_number            <- map["order_number"]
        invoice_company         <- map["invoice_company"]
        invoice_number          <- map["invoice_number"]
        challan_number          <- map["challan_number"]
        igst                    <- map["igst"]
        cgst                    <- map["cgst"]
        sgst                    <- map["sgst"]
        customer_email          <- map["customer_email"]
        send_quantity           <- map["send_quantity"]
        product_id              <- map["product_id"]
        dispatch_date           <- map["dispatch_date"]
        dispatch_through        <- map["dispatch_through"]
        status                  <- map["status"]
        price                   <- map["price"]
        amount                  <- map["amount"]
        packing_material        <- map["packing_material"]
        packing                 <- map["packing"]
        category_id             <- map["category_id"]
        recevied_amount         <- map["recevied_amount"]
        packing_hsn             <- map["packing_hsn"]
        ir_number               <- map["lr_number"]
        delivery_date           <- map["delivery_date"]
        invoice_with_tax        <- map["invoice_with_tax"]
      
    }
    
    
  
}
